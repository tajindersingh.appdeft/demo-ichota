/**
 * @author Kishor Mali
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	

	jQuery(document).on("click", ".deleteCategory", function(){
		var categoryId = $(this).data("categoryid"),
			hitURL = baseURL + "deleteCategory",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this Category ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { categoryId : categoryId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Category successfully deleted"); }
				else if(data.status = false) { alert("Category deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	

	
	jQuery(document).on("click", ".deleteVideo", function(){
		var videoId = $(this).data("videoid"),
			hitURL = baseURL + "deleteVideo",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this Video ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { videoId : videoId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Video successfully deleted"); }
				else if(data.status = false) { alert("Video deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});


	
	jQuery(document).on("click", ".deleteRj", function(){
		var rjId = $(this).data("rjid"),
			hitURL = baseURL + "deleteRj",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this RJ ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { rjId : rjId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("RJ successfully deleted"); }
				else if(data.status = false) { alert("RJ deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});



	jQuery(document).on("click", ".deleteprogram", function(){
		var programId = $(this).data("programid"),
			hitURL = baseURL + "deleteprogram",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this Program ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { programId : programId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Program successfully deleted"); }
				else if(data.status = false) { alert("Program deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});


	jQuery(document).on("click", ".deletead_management", function(){
		var adId = $(this).data("adid"),
			hitURL = baseURL + "deletead_management",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this AD ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { adId : adId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("AD successfully deleted"); }
				else if(data.status = false) { alert("AD deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});




	jQuery(document).on("click", ".deletestreamfm", function(){
		var streamfmId = $(this).data("streamfmid"),
			hitURL = baseURL + "deletestreamfm",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this Stream FM ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { streamfmId : streamfmId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Stream FM successfully deleted"); }
				else if(data.status = false) { alert("Stream FM deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});


	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
