/**
 * File : editad_management.js 
 * 
 * This file contain the validation of edit AD form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editAdForm = $("#editad_management");
	
	var validator = editAdForm.validate({
		
		rules:{
			fname :{ required : true },
			price : { required : true }
		},
		messages:{
			fname :{ required : "This field is required" },
			price :{ required : "This field is required" }			
		}
	});


});