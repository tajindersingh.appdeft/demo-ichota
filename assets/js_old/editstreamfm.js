/**
 * File : editstreamfm.js 
 * 
 * This file contain the validation of edit Stream FM form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editstreamfmForm = $("#editstreamfm");
	
	var validator = editstreamfmForm.validate({
		
		rules:{
			fname :{ required : true },
			url : { required : true, }
		},
		messages:{
			fname :{ required : "This field is required" },
			url :{ required : "This field is required" }			
		}
	});


});