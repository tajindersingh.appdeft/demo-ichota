/**
 * File : addprogram.js
 * 
 * This file contain the validation of add program form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addprogramForm = $("#addprogram");
	
	var validator = addprogramForm.validate({
		
		rules:{
			fname :{ required : true },
			title : { required : true },
			rjid : { required : true, }
		},
		messages:{
			fname :{ required : "This field is required" },
			title :{ required : "This field is required" },
			rjid :{ required : "This field is required" }		
		}
	});
});
