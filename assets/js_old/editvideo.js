/**
 * File : editvideo.js 
 * 
 * This file contain the validation of edit video form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editvideoForm = $("#editvideo");
	
	var validator = editvideoForm.validate({
		
		rules:{
			fname :{ required : true },
			title : { required : true },
			url : { required : true, },
			categoryid : { required : true, }
		},
		messages:{
			fname :{ required : "This field is required" },
			title :{ required : "This field is required" },
			url :{ required : "This field is required" },
			categoryid :{ required : "This field is required" }			
		}
	});


});