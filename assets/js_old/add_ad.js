/**
 * File : add_ad.js
 * 
 * This file contain the validation of add AD form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addAdForm = $("#add_ad");
	
	var validator = addAdForm.validate({
		
		rules:{
			fname :{ required : true },
			price : { required : true }
		},
		messages:{
			fname :{ required : "This field is required" },
			price :{ required : "This field is required" }		
		}
	});
});
