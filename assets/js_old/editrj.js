/**
 * File : editrj.js 
 * 
 * This file contain the validation of edit rj form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editrjForm = $("#editrj");
	
	var validator = editrjForm.validate({
		
		rules:{
			fname :{ required : true }
		},
		messages:{
			fname :{ required : "This field is required" }			
		}
	});


});