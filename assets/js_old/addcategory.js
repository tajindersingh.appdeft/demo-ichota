/**
 * File : addcategory.js
 * 
 * This file contain the validation of add category form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addcategoryForm = $("#addcategory");
	
	var validator = addcategoryForm.validate({
		
		rules:{
			fname :{ required : true }
		},
		messages:{
			fname :{ required : "This field is required" }			
		}
	});
});
