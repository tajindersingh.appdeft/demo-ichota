/**
 * File : addrj.js
 * 
 * This file contain the validation of add rj form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addrjForm = $("#addrj");
	
	var validator = addrjForm.validate({
		
		rules:{
			fname :{ required : true }
		},
		messages:{
			fname :{ required : "This field is required" }			
		}
	});
});
