/**
 * File : editcategory.js 
 * 
 * This file contain the validation of edit category form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editcategoryForm = $("#editcategory");
	
	var validator = editcategoryForm.validate({
		
		rules:{
			fname :{ required : true }
		},
		messages:{
			fname :{ required : "This field is required" }			
		}
	});


});