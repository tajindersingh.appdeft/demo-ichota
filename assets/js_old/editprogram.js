/**
 * File : editprogram.js 
 * 
 * This file contain the validation of edit program form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editprogramForm = $("#editprogram");
	
	var validator = editprogramForm.validate({
		
		rules:{
			fname :{ required : true },
			title : { required : true },
			rjid : { required : true, }
		},
		messages:{
			fname :{ required : "This field is required" },
			title :{ required : "This field is required" },
			rjid :{ required : "This field is required" }			
		}
	});


});