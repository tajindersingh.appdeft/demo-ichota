<?php
$videoId = $videoInfo->id;
$video_name = $videoInfo->video_name;
$title = $videoInfo->title;
$image = $videoInfo->image;
$url = $videoInfo->url;
$description = $videoInfo->description;
$category_id = $videoInfo->category_id;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Video
        <small>Add / Edit Video</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Video Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>editvideo" method="post" id="editvideo" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $video_name; ?>" maxlength="256">
                                        <input type="hidden" value="<?php echo $videoId; ?>" name="videoId" id="videoId" />    
                                    </div>
                                    
                                </div>
								 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" placeholder="Title" value="<?php echo $title; ?>" id="title" name="title" maxlength="256">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="url">URL</label>
                                        <input type="text" class="form-control" placeholder="URL" value="<?php echo $url; ?>" id="url" name="url" maxlength="256">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="categoryid">Category</label>
                                        <select placeholder="Select Category ID " name="categoryid" id="categoryid" class="form-control required" >
												<option value="">Select Category</option>
												 <?php
												 foreach($categoryRecords as $record)
													{
														if($record->isactive == 1)
														{
															echo " <option  value='".$record->id."' ";
															
																if($record->id == $category_id)
																{
																	echo "selected";
																}
															
															
															echo "	>".$record->category_name."</option>";
														}
													}
													?>
										</select>
                                    </div>
                                </div>
                            </div>
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="image"  maxlength="256">
                                    </div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
										<img src="<?php echo(site_url('assets/upload/'.$image));?>" id="imagepreview" name="imagepreview" width="42" height="42">
									</div>
								</div>
                            </div>
							
							<div class="row">
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text" class="form-control " placeholder="Description" value="<?php echo $description; ?>" id="description" name="description" maxlength="256">
                                    </div>
                                </div>
                            </div>
							
							
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editvideo.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagepreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function(){
    readURL(this);
});
</script>



