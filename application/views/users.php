<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>userListing?type=request-page"><i class="fa fa-plus"></i> Approve / Reject </a>
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>userListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Created On</th>
						<?php if($page_type == "request-page")
						{?>
							<th>Request Approve</th>
					<?php	}
						else
						{?>
							<th>Status</th>
						<?php }
						?>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
                        <td><?php echo $record->name ?></td>
                        <td><?php echo $record->email ?></td>
                        <td><?php echo $record->mobile ?></td>
                        <td><?php echo $record->role ?></td>
                        <td><?php echo date("d-m-Y", strtotime($record->createdDtm)) ?></td>
						<?php if($page_type == "request-page")
						{ 
					?>
					<td><input type="button" data-status="1" class="request_action" data-action="<?php echo $page_type; ?>" value="approved" data-userid="<?php echo $record->userId; ?>"><input class="request_action" data-status="2" type="button" data-action="<?php echo $page_type; ?>" value="rejected" data-userid="<?php echo $record->userId; ?>"></td>
					<?php }
						else
						{ ?>
							<td><input type="checkbox" data-action="" data-userid="<?php echo $record->userId; ?>" <?php if($record->isactive==1){ echo('checked="checked"');} ?> class="ischecked" data-toggle="toggle" ></td>
					<?php } ?>
						
                        <td class="text-center">
                            <a class="btn btn-sm btn-primary" href="<?= base_url().'login-history/'.$record->userId; ?>" title="Login history"><i class="fa fa-history"></i></a> | 
                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'editOld/'.$record->userId; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="<?php echo $record->userId; ?>" title="Delete"><i class="fa fa-trash"></i></a>
							
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
		$(".ischecked").change(function(){
			
		var userId = $(this).data("userid"),
			hitURL = baseURL + "isactivate",
			currentRow = $(this);
		
        if($(this).is(":checked")){
		var status=1;
		var message="Activate";		
		}
		else{
		var status=0;
		var message="Deactivate";		
		}
	
		
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId,status:status } 
			}).done(function(data){
				console.log(data);
				
				if(data.status = true) { alert("User successfully "+message); }
				else if(data.status = false) { alert("User "+message+" failed"); }
				else { alert("Access denied..!"); }
			});
	
      
    })
	
	$(".request_action").click(function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "isactivate",
			currentRow = $(this);
		
		var status = $(this).data("status");
		var action = $(this).data("action");
		
        if(status == '1')
		{
		var message="Request Approved";	
		}
		if(status == '2')
		{
		var message="Request Reject";	
		}
		
		var confirmation = confirm("Are you sure to "+message+" this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId,status:status,action:action } 
			}).done(function(data){
				console.log(data);
				
				if(data.status = true) { alert("User successfully "+message); }
				else if(data.status = false) { alert("User "+message+" failed"); }
				else { alert("Access denied..!"); }
			});
		}
      
    })
    });
</script>
