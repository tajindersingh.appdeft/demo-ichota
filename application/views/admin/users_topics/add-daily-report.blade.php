<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Add Topic Report</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Category</li>
                </ol> -->

                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Add User
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                               <div class="container">
                                  <!-- <h2>Vertical (basic) form</h2> -->
                                  <form name="add_report" action="{{ route('add_user.topic_report') }}" class="form-inline" method="post">
                                    @csrf
                                    <!-- {{ method_field('PUT') }} -->
                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif
                                  <div class="form-row">
                                    <div class="col">
                                       <label for="score">Select Category:</label><span style="color:#ff0000">*</span>
                                      <select class="form-control" id="cat_id" name="cat_id">
                                          @if($result)
                                          @foreach($result as $val)
                                                  <option value="{{$val->cat_id}}">{{ $val->cat_name }}</option>
                                          @endforeach
                                          @endif
                                      </select>
                                    </div>
                                    <div class="col">
                                       <label for="score">Score:</label><span style="color:#ff0000">*</span>
                                      <input type="number" class="form-control" id="score" placeholder="Score" name="score" min="1" max="100">
                                      <span style="color: red">@error("score"){{($message)}}@enderror</span>
                                    </div>
                                  </div><br>
                                  <div class="form-row">
                                    <div class="col">
                                       <label for="Upbringing">Upbringing:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="upbringing" id="upbringing" placeholder="Upbringing"></textarea>
                                       <span style="color: red">@error("upbringing"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="col">
                                       <label for="Influence">Influence:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="influence" id="influence" placeholder="Influence"></textarea> 
                                      <span style="color: red">@error("influence"){{($message)}}@enderror</span>
                                    </div>
                                  </div><br>

                                  <div class="form-row">
                                    <div class="col">
                                       <label for="Derived">Derived:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="derived" id="derived" placeholder="Derived"></textarea>
                                       <span style="color: red">@error("derived"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="col">
                                       <label for="Indoctrination">Indoctrination:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="indoctrination" id="indoctrination" placeholder="Indoctrination"></textarea>
                                       <span style="color: red">@error("indoctrination"){{($message)}}@enderror</span>
                                    </div>
                                  </div><br>
                                   <div class="form-row">
                                    <div class="col">
                                       <label for="Afirmation">Afirmation:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="afirmation" id="afirmation" placeholder="Afirmation"></textarea>
                                       <span style="color: red">@error("afirmation"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="col">
                                       <label for="Reward">Reward:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="reward" id="reward" placeholder="Reward"></textarea>
                                       <span style="color: red">@error("reward"){{($message)}}@enderror</span>
                                    </div>
                                  </div><br>
                                   <div class="form-row">
                                    <div class="col">
                                       <label for="Promote">Promote:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="promote" id="promote" placeholder="Promote"></textarea>
                                       <span style="color: red">@error("promote"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="col">
                                       <label for="Enforce">Enforce:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="enforce" id="enforce" placeholder="Enforce"></textarea>
                                       <span style="color: red">@error("enforce"){{($message)}}@enderror</span>
                                    </div>
                                  </div><br>
                                   <div class="form-row">
                                    <div class="col">
                                       <label for="Parroting">Parroting:</label><span style="color:#ff0000">*</span>
                                      <textarea class="form-control" name="parroting" id="parroting" placeholder="parroting"></textarea>
                                       <span style="color: red">@error("parroting"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="col"></div>
                                  </div><br>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </form>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />
    <script type="text/javascript">
        $("#add_report").validate({
            rules: {
                upbringing: {minlength: 1, maxlength: 100, required: !0},
                last_name: {minlength: 1, maxlength: 100, required: !0},
                email: {email: true, minlength: 1, maxlength: 100, required: !0},
                password: {minlength: 1, required: !0},
                phone_no: { minlength: 8, required: !0}
            },
            messages:{
                upbringing:"The first name is required",
                last_name:"The last name is required",
                email:"The email is required",
                password:"The password is required",
                phone_no:"The phone_no is required",
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    </script>
<!-- end validations -->