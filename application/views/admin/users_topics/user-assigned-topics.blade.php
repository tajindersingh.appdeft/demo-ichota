<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Assigned Topics</h1>
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Assigned Topics
                    </div>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>User Name</th>
                                        <th>Assigned Topics</th>
                                        <th>Topic Score</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($result) && !empty($result))
                                    @foreach($result as $row)
                                    <tr>
                                        <td>{{$row->user_id}}</td>
                                        <td>{{$row->first_name}} {{$row->last_name}}</td>
                                        <td>{{$row->cat_name}}</td>
                                        <td>{{$row->cat_name}}</td>
                                        <td>{{$row->cat_name}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td>Data Not Found</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />