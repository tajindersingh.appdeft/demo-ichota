<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">User Details</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Users</li>
                </ol> -->
                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Users
                    </div> -->
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="sortable-table-2" class="table table-striped data-table">
                                <tbody>
                                    @if(isset($result->first_name) && !empty($result->first_name))

                                    
                                        <tr>
                                            <th>First Name</th>
                                            <td> {{ $result->first_name }} </td>
                                        </tr>
                                        <tr>
                                            <th>Last Name</th>
                                            <td>{{ $result->last_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td> {{ $result->email }} </td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td>{{ $result->phone }}</td>
                                        </tr>
                                        <tr>
                                            <th>User Role</th>
                                            <td>
                                            @if($result->user_type == 1){{'Admin'}} @elseif($result->user_type == 2){{ 'User' }} @endif
                                        </td>
                                        </tr>
                                        <tr>
                                            <th>Created At</th>
                                            <td>{{ date("d-m-Y",strtotime($result->created_at))}}</td>
                                        </tr>
                                        @elseif(!isset($result) && empty($result))
                                        <tr>
                                            <th>Data Not Found</th>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                <a href="{{url('users')}}" class="btn btn-primary">Back</a>
            </div>
        </main>
<x-footer />
