<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Add Users</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Users</li>
                </ol> -->

                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Add User
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                               <div class="container">
                                  <!-- <h2>Vertical (basic) form</h2> -->
                                  <form action="{{ route('user.add') }}" class="form-inline" method="post" id="store">
                                    @csrf
                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif
                                    <div class="form-group">
                                      <label for="first_name">First Name:</label>
                                      <input type="text" class="form-control inpt" id="first_name" placeholder="Enter first name" name="first_name">
                                      <span style="color: red">@error("first_name"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="form-group">
                                      <label for="last_name">Last Name:</label>
                                      <input type="text" class="form-control inpt" id="last_name" placeholder="Enter last name" name="last_name">
                                       <span style="color: red">@error("last_name"){{($message)}}@enderror</span>
                                    </div>
                                     <div class="form-group">
                                      <label for="email">Email:</label>
                                      <input type="email" class="form-control inpt" id="email" placeholder="Enter email" name="email">
                                      <span style="color: red">@error("email"){{($message)}}@enderror</span>
                                    </div>
                                     <div class="form-group">
                                      <label for="password">Password:</label>
                                      <input type="password" class="form-control inpt" id="password" placeholder="Enter password" name="password">
                                       <span style="color: red">@error("password"){{($message)}}@enderror</span>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </form>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />
