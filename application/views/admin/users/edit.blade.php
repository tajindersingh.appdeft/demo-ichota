<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Update User</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Users</li>
                </ol> -->

                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Add User
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                               <div class="container">
                                  <!-- <h2> Update User form</h2> -->
                                  @if($result)
                                  <form action="{{ route('user.update',$result->id) }}" class="form-inline" method="post">
                                    <!-- csrf token  -->
                                    @csrf
                                    <div class="form-group">
                                      <label for="first_name">First Name:</label>
                                      <input type="text" class="form-control inpt" id="first_name" placeholder="Enter first name" name="first_name" value="{{$result->first_name}}">
                                      <span style="color: red">@error("first_name"){{($message)}}@enderror</span>
                                    </div>
                                    <div class="form-group">
                                      <label for="last_name">Last Name:</label>
                                      <input type="text" class="form-control inpt" id="last_name" placeholder="Enter last name" name="last_name" value="{{$result->last_name}}">
                                       <span style="color: red">@error("last_name"){{($message)}}@enderror</span>
                                    </div>
                                     <div class="form-group">
                                      <label for="password">Password:</label>
                                      <input type="password" class="form-control inpt" id="password" placeholder="Enter password" name="password" value="{{$result->password}}">
                                       <span style="color: red">@error("password"){{($message)}}@enderror</span>
                                    </div>
                                    <input type="hidden" name="user_id" value="{{$result->id}}">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                  </form>
                                  @endif
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />