<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Users</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Users</li>
                </ol> -->
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Users
                    </div>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>User Role</th>
                                        <th>Created date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($results) && !empty($results))
                                    @foreach($results as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->first_name}} {{$row->last_name}}</td>
                                        <td>{{$row->email}}</td>
                                        <td>@if($row->user_type == 1){{'Admin'}} @elseif($row->user_type == 2){{ 'User' }} @endif</td>
                                        <td>{{ date("d-m-Y",strtotime($row->created_at))}}</td>
                                        <td> <a name="view" href="{{ route('user.view', ['user_id' => $row->id]) }}" class="btn btn-info">View</a>
                                        <a name="edit" href="{{ route('user.edit', ['user_id' => $row->id]) }}" class="btn btn-primary">Edit</a>
                                        <a name="delete" href="{{ route('user.delete', ['user_id' => $row->id]) }}" class="btn btn-danger">Delete
                                        </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />