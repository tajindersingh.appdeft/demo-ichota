<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Reports</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Reports</li>
                </ol> -->
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Reports
                    </div>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <!-- <th>ID</th> -->
                                        <th>User Name</th>
                                        <th>Topic</th>
                                        <th>Score</th>
                                        <th>Upbringing</th>
                                        <th>Influence</th>
                                        <th>Derived</th>
                                        <th>Indoctrination</th>
                                        <th>Afirmation</th>
                                        <th>Reward</th>
                                        <th>Promote</th>
                                        <th>Enforce</th>
                                        <th>Parroting</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($results) && !empty($results))
                                    @foreach($results as $row)
                                    <tr>
                                        <td>{{ $row->first_name }} {{ $row->last_name }}</td>
                                        <td>{{$row->topic_name}}</td>
                                        <td>{{$row->score}}</td>
                                        <td>{{mb_strimwidth($row->upbringing, 0, 75, "...")}}</td>
                                        <td>{{mb_strimwidth($row->influence, 0, 75, "...")}}</td>
                                        <td>{{mb_strimwidth($row->derived, 0, 75, "...")}}</td>
                                        <td>{{mb_strimwidth($row->indoctrination, 0, 75, "...")}}</td>
                                        <td>{{ mb_strimwidth($row->afirmation, 0, 75, "...")}}</td>
                                        <td>{{ mb_strimwidth($row->reward, 0, 75, "...")}}</td>
                                        <td>{{ mb_strimwidth($row->promote, 0, 75, "...")}}</td>
                                        <td>{{ mb_strimwidth($row->enforce, 0, 75, "...")}}</td>
                                        <td>{{ mb_strimwidth($row->parroting, 0, 75, "...")}}</td>
                                        <td>{{ date("d-m-Y",strtotime($row->created_at))}}</td>
                                        <td> <a name="view" href="#" class="btn btn-info">View</a>
                                        <a name="edit" href="#" class="btn btn-primary">Edit</a>
                                        <a name="delete" href="#" class="btn btn-danger" style="margin-top: -63px;margin-left: 125px;">Delete
                                        </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />