<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Add Category</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Category</li>
                </ol> -->

                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Add User
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                               <div class="container">
                                  <!-- <h2>Vertical (basic) form</h2> -->
                                  <form action="{{ route('cat.add') }}" class="form-inline" method="post" id="store">
                                    @csrf
                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif
                                    <div class="form-group">
                                      <label for="cat_name">Category Name:</label>
                                      <input type="text" class="form-control inpt" id="cat_name" placeholder="Enter category name" name="cat_name">
                                      <span style="color: red">@error("cat_name"){{($message)}}@enderror</span>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </form>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />
