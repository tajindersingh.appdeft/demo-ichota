<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Categories</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Categories</li>
                </ol> -->
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Categories
                    </div>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Category ID</th>
                                        <th>Category Name</th>
                                        <th>Created date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($results) && !empty($results))
                                    @foreach($results as $row)
                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{ date("d-m-Y",strtotime($row->created_at))}}</td>
                                        <td> <a name="view" href="{{ url('admin/categories/view')}}/{{$row->id }}" class="btn btn-info">View</a>
                                        <a name="edit" href="{{ url('admin/categories/edit')}}/{{$row->id}}" class="btn btn-primary">Edit</a>
                                        <a name="delete" href="{{ url('admin/categories/delete') }}/{{$row->id}}" class="btn btn-danger">Delete
                                        </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />