<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Category Details</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Users</li>
                </ol> -->
                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Users
                    </div> -->
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="sortable-table-2" class="table table-striped data-table">
                                <tbody>
                                    @if(isset($result->name) && !empty($result->name))
                                        <tr>
                                            <th>Category Name</th>
                                            <td> {{ $result->name }} </td>
                                        </tr>
                                        <tr>
                                            <th>Created At</th>
                                            <td>{{ date("d-m-Y",strtotime($result->created_at))}}</td>
                                        </tr>
                                        @elseif(!isset($result) && empty($result))
                                        <tr>
                                            <th>Data Not Found</th>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                <a href="{{url('admin/categories')}}" class="btn btn-primary">Back</a>
            </div>
        </main>
<x-footer />
