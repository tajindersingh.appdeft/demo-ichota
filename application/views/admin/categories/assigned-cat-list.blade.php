<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Assigned Categories</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Assigned Categories</li>
                </ol> -->
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Assigned Categories
                    </div>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>User Name</th>
                                        <th>Category</th>
                                        <th>Created date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($result) && !empty($result))
                                    @foreach($result as $row)
                                    <tr>
                                        <td>{{$row->user_id}}</td>
                                        <td>{{$row->first_name}} {{$row->last_name}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{ date("d-m-Y",strtotime($row->created_at))}}</td>
                                        <td>

                                        <!--  <a name="view" href="{{ route('user.view', ['user_id' => $row->id]) }}" class="btn btn-info">View</a>
                                        <a name="edit" href="{{ route('user.edit', ['user_id' => $row->id]) }}" class="btn btn-primary">Edit</a> -->


                                        <a name="delete" href="{{ url('admin/categories/assigned-categories/delete/')}}/{{$row->id}}" class="btn btn-danger">Delete
                                        </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td>Data Not Found</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />