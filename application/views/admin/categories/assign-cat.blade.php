<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Assign Category</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Category</li>
                </ol> -->

                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Add User
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                               <div class="container">
                                  <!-- <h2>Vertical (basic) form</h2> -->
                                  <form action="{{ route('cat.assign_cat_save') }}" class="form-inline" method="post">
                                    @csrf
                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif
                                    <div class="form-group">
                                      <label for="cat">Select Category:</label>
                                          <select class="form-control" id="cat_id" name="cat_id" style="width:192px;">
                                    @if($cat)
                                    @foreach($cat as $val)
                                            <option value="{{$val->id}}">{{ $val->name }}</option>
                                    @endforeach
                                    @endif
                                          </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="user">Select User:</label>
                                          <select class="form-control" id="user_id" name="user_id" style="width:192px;">
                                    @if($users)
                                    @foreach($users as $val)
                                            <option value="{{ $val->id }}">{{ $val->first_name }} {{ $val->last_name }}</option>
                                    @endforeach
                                    @endif
                                          </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </form>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />