<x-header />
<div id="layoutSidenav">
<x-sidebar />
</div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Update Category</h1>
                <!--  <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Category</li>
                </ol> -->

                <div class="card mb-4">
                    <!-- <div class="card-header">
                        <i class="fas fa-table mr-1"></i>
                        Add User
                    </div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                               <div class="container">
                                  <!-- <h2> Update User form</h2> -->
                                  @if($result)
                                  <form action="{{ url('admin/categories/update') }}/{{$result->id}}" class="form-inline" method="post">
                                    <!-- csrf token  -->
                                    @csrf
                                    <div class="form-group">
                                      <label for="cat_name">Category Name:</label>
                                      <input type="text" class="form-control inpt" id="cat_name" placeholder="Enter first name" name="cat_name" value="{{$result->name}}">
                                      <span style="color: red">@error("cat_name"){{($message)}}@enderror</span>
                                    </div>
                                    <input type="hidden" name="user_id" value="{{$result->id}}">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                  </form>
                                  @endif
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<x-footer />