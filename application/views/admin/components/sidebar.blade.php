<?php 
$sessionDetails = session()->all();
if (isset($sessionDetails['user']) && !empty($sessionDetails['user'])) {
    $name = $sessionDetails['user']['first_name'].' '. $sessionDetails['user']['last_name'];
}
?>
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading" style="color: #f7faff;">{{ $name }}</div>
                <div class="sb-sidenav-menu-heading">Menus</div>
                <a class="nav-link" href="{{( url('home') )}}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                <!--  if user role is user only  -->
                @if(isset($sessionDetails['user']) && $sessionDetails['user']['user_type'] == 2)
                <a class="nav-link" href="{{( url('admin/topics/assigned-topics') )}}">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Assigned Topics
                </a>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                   Daily Reports
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{( route('get.topic_report') )}}">List</a>
                        <a class="nav-link" href="{{( route('add.topic_report') )}}">Add Daily Report</a>
                    </nav>
                </div>

                @endif
                <!-- end -->

                <!-- if user is admin -->
                @if(isset($sessionDetails['user']) && $sessionDetails['user']['user_type'] == 1)
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Manage Users
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{( url('users') )}}">Users List</a>
                        <a class="nav-link" href="{{( url('add-user') )}}">Add New User</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts_cat" aria-expanded="false" aria-controls="collapseLayouts_cat">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Manage Categories
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts_cat" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{( url('admin/categories') )}}">Categories List</a>
                        <a class="nav-link" href="{{( url('admin/categories/add') )}}">Add New Category</a>
                         <a class="nav-link" href="{{( url('admin/categories/assign-category-to-user') )}}">Assign Category</a>
                         <a class="nav-link" href="{{( route('cat.get.assign_cat') )}}">Assigned Categories</a>
                    </nav>
                </div>

                 <a class="nav-link" href="{{( route('get.topic_report') )}}">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Manage Reports
                </a>


                <a class="nav-link" href="{{( url('contributions') )}}">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Manage Contributions
                </a>

                <a class="nav-link" href="{{( url('donnations') )}}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                   Manage Donations 
                </a>
                @endif
                <!-- end -->

            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small" style="font-size: 17px;">Logged in as:</div>
            {{ $name }}
        </div>
        <div class="sb-sidenav-footer">
            <div class="small"><a href="{{( url('logout') )}}" style="color: grey; font-size: 17px;">Logout</a></div>
        </div>
    </nav>
