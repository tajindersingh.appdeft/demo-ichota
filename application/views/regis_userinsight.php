<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-video"></i> Registered User Insight
        
      </h1>
    </section>
	<section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
					
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Registered User Insight List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>regis_userinsight" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                        <th>Email</th>
						<th>Mobile</th>
                        <th>Gender</th>
                    </tr>
                    <?php
                    if(!empty($regis_userinsightRecords))
                    {
                        foreach($regis_userinsightRecords as $record)
                        {
                    ?>
                    <tr>
                        <td><?php echo $record->email ?></td>
						<td><?php echo $record->mobile ?></td>
                        <td><?php echo $record->gender ?></td>
                        
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
					<div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
    
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "regis_userinsight/" + value);
            jQuery("#searchList").submit();
        });
	
    });
</script>


