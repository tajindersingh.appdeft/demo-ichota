<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Program
        <small>Add / Edit Program</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Program Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" enctype="multipart/form-data" id="addprogram" action="<?php echo base_url() ?>addNewprogram" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Program Name</label>
                                        <input type="text" class="form-control required" value="" id="fname" name="fname" maxlength="256">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control required" value="" id="title" name="title" maxlength="256">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control required" id="image" name="image"  maxlength="256">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rjid">RJ</label>
                                        <select placeholder="Select RJ ID " name="rjid" id="rjid" class="form-control required" >
												<option value="">Select Category</option>
												 <?php
												 foreach($rjRecords as $record)
													{
														if($record->isactive == 1)
														{
															echo " <option value='".$record->id."'>".$record->rj_name."</option>";
														}
													
													}
													?>
										</select>
                                    </div>
                                </div>
                            </div>	
							
							<div class="row">
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="starttime">StartTime</label>
                                        <input type="time" class="form-control required" value="" id="starttime" name="starttime" maxlength="256">
                                    </div>
                                </div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="endtime">EndTime</label>
                                        <input type="time" class="form-control required" value="" id="endtime" name="endtime" maxlength="256">
                                    </div>
                                </div>
                            </div>
							
							<div class="row">
								<div class="col-md-6">
									<label for="datetime">DateTime</label>
										<div class='input-group date' id='dateValue'>
										  <input type='text' class="form-control datetime required" name="datetime" id="datetime" />
										  <span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										  </span>
										</div>
								</div>
                            </div>
							
							
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addprogram.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#dateValue").datetimepicker({ format: 'YYYY-MM-DD',});
});
</script> 