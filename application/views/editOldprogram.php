<?php
$programId = $programInfo->id;
$program_name = $programInfo->program_name;
$title = $programInfo->title;
$image = $programInfo->image;
$rj_id = $programInfo->rj_id;
$datetime = $programInfo->datetime;
?>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Program
        <small>Add / Edit Program</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Program Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>editprogram" method="post" id="editprogram" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $program_name; ?>" maxlength="256">
                                        <input type="hidden" value="<?php echo $programId; ?>" name="programId" id="programId" />    
                                    </div>
                                    
                                </div>
								 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" placeholder="Title" value="<?php echo $title; ?>" id="title" name="title" maxlength="256">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rjid">RJ</label>
                                        <select placeholder="Select RJ ID " name="rjid" id="rjid" class="form-control required" >
												<option value="">Select RJ</option>
												 <?php
												 foreach($rjRecords as $record)
													{
														if($record->isactive == 1)
														{
															echo " <option  value='".$record->id."' ";
															
																if($record->id == $rj_id)
																{
																	echo "selected";
																}
															
															
															echo "	>".$record->rj_name."</option>";
														}
													}
													?>
										</select>
                                    </div>
                                </div>
                            </div>
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="image"  maxlength="256">
                                    </div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
										<img src="<?php echo(site_url('assets/upload/'.$image));?>" id="imagepreview" name="imagepreview" width="42" height="42">
									</div>
								</div>
                            </div>
							
							<div class="row">
								<div class="col-md-6">
								<label for="datetime">DateTime</label>
									<div class='input-group date' id='dateValue'>
									  <input type='text' value="<?php echo $datetime; ?>" class="form-control datetime required" name="datetime" id="datetime" />
									  <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									  </span>
									</div>
								</div>
                            </div>
							
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editprogram.js" type="text/javascript"></script>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagepreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function(){
    readURL(this);
});

$(document).ready(function() {
    $("#dateValue").datetimepicker({ format: 'YYYY-MM-DD hh:ss',});
});

</script>

