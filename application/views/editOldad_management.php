<?php
$adId = $adInfo->id;
$ad_name = $adInfo->ad_name;
$price = $adInfo->price;
$ad_image = $adInfo->ad_image;
$display_date = $adInfo->display_date;
$expiry_date = $adInfo->expiry_date;
?>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> AD
        <small>Add / Edit AD</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter AD Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>editad_management" method="post" id="editad_management" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $ad_name; ?>" maxlength="256">
                                        <input type="hidden" value="<?php echo $adId; ?>" name="adId" id="adId" />    
                                    </div>
                                    
                                </div>
								 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" class="form-control" placeholder="Price" value="<?php echo $price; ?>" id="price" name="price" maxlength="256">
                                    </div>
                                </div>
                            </div>
							
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="image"  maxlength="256">
                                    </div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
										<img src="<?php echo(site_url('assets/upload/'.$ad_image));?>" id="imagepreview" name="imagepreview" width="42" height="42">
									</div>
								</div>
                            </div>
							
							<div class="row">
								<div class="col-md-6">
									<label for="displaydate">Display Date</label>
										<div class='input-group date' id='dateValue1'>
										  <input type='text' value="<?php echo $display_date; ?>" class="form-control displaydate required" name="displaydate" id="displaydate" />
										  <span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										  </span>
										</div>
								</div>
								<div class="col-md-6">
									<label for="expirydate">Expiry Date</label>
										<div class='input-group date' id='dateValue2'>
										  <input type='text' value="<?php echo $expiry_date; ?>" class="form-control expirydate required" name="expirydate" id="expirydate" />
										  <span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										  </span>
										</div>
								</div>
                            </div>
							
							
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editad_management.js" type="text/javascript"></script>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagepreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function(){
    readURL(this);
});

$(document).ready(function() {
    $("#dateValue1").datetimepicker({ format: 'YYYY-MM-DD',});
	$("#dateValue2").datetimepicker({ format: 'YYYY-MM-DD',});
});

</script>
