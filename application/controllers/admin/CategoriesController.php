<?php
  
namespace App\Http\Controllers;
   
use App\Models\admin\CategoryModel;
use App\Models\admin\UserModel;
use Illuminate\Http\Request;
use Session;
  
class CategoriesController extends Controller
{
    /**
     *index function
     * 
     * This function is called to load categories listing page.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */
    public function index()
    {
        $cat = CategoryModel::where('deleted_at', NULL)->get();
        return view('admin.categories/list', ['results'=> $cat]);
    }

    /**
     *add function
     * 
     * This function is called to add new user in database.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function add(Request $request){
    	
    	$request->validate(["cat_name"=> "required"]);
		$data =array(
			"name" => $request->cat_name
		);
		$obj = new CategoryModel();
		$save = $obj->add($data);
		if (isset($save) && !empty($save)) {
			Session::flash('message', 'The category has been added successfully'); 
			Session::flash('alert-class', 'alert-success'); 
			return redirect('admin/categories/');
		}else{
			Session::flash('message', 'Sorry! Something went wrong'); 
			Session::flash('alert-class', 'alert-danger'); 
			return redirect('admin/categories/add/');
		}
    }
    /**
     * view function
     * 
     * This function is called to view user details.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function view(){
    	/* get user id */
 		$cat_id =request()->segments();
    	$result = CategoryModel::find($cat_id[3]);
		return view('admin.categories.show',['result'=> $result]);
    }



      /**
     * edit function
     * 
     * This function is called to edit user.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function edit(Request $request){
    	/* get category id */
    	$cat_id =request()->segments();
    	$result = CategoryModel::find($cat_id[3]);
		if (isset($result) && !empty($result)) {
			return view('admin.categories.edit',compact('result', $result));
		}else{

		}
    }

      /**
     * update function
     * 
     * This function is called to update user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function update(Request $request){
    	
    	$request->validate(["cat_name"=>"required"]);
		$data = array(
			"name"=>$request->cat_name
		);
		$cat_id =request()->segments();
		$obj = new CategoryModel();
		$result = $obj->updateCat($data, $cat_id[3]);
		if (isset($result) && $result != false) {
            Session::flash('message', 'The category has been updated successfully'); 
            Session::flash('alert-class', 'alert-success');
			return redirect('admin/categories');
		}else{
            Session::flash('message', 'Sorry! Something went wrong'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/categories/add');
		}
    }

      /**
     * deleteCat function
     * 
     * This function is called to delete user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function deleteCat(Request $request){
        
        /* get user id */
        $cat_id =request()->segments();
        $obj = new CategoryModel();
        $result = $obj->deleteCat($cat_id[3]);
        if (isset($result) && $result != false) {
            Session::flash('message', 'The category has been deleted successfully'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('admin/categories');
        }else{
            Session::flash('message', 'Sorry! Something went wrong'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/categories');
        }
    }

    /**
     *showCatPage function
     * 
     * This function is called to load assign category page.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */
    public function showCatPage()
    {
        $cat = CategoryModel::where('deleted_at', NULL)->get();
        $users = UserModel::where('deleted_at', NULL)->get();
        return view('admin.categories/assign-cat', ['cat'=> $cat, 'users'=>$users]);
    }


     /**
     * assignCatToUsers function
     * 
     * This function is called to delete user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function assignCatToUsers(Request $request){

    	$data = array(
    		'category_id' => $request->cat_id,
    		'user_id' => $request->user_id
    	);
        
        $obj = new CategoryModel();
        $checkuser = $obj->checkAssignedCat($data);

        if ($checkuser == false) {
            $result = $obj->assignCatToUsers($data);
            if (isset($result) && $result != false) {
                Session::flash('message', 'The category has been assigned successfully'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('admin/categories/assigned-categories');
            }else{
                Session::flash('message', 'Sorry! Something went wrong'); 
                Session::flash('alert-class', 'alert-danger'); 
                return redirect('admin/categories/assigned-categories');
            }
        }elseif ($checkuser == true) {
            Session::flash('message', 'Category is already assigned to this user'); 
                Session::flash('alert-class', 'alert-danger'); 
                return redirect('admin/categories/assign-category-to-user');
        }
    }
     


    /**
     * getAssignedCat function
     * 
     * This function is called to delete user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function getAssignedCat(Request $request){

        $obj = new CategoryModel();
        $result = $obj->getAssignedCat();
        return view('admin.categories.assigned-cat-list', compact('result'));
    }


         /**
     * deleteAssignedCat function
     * 
     * This function is called to delete user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function deleteAssignedCat(Request $request){
        
        /* get user id */
        $cat_id =request()->segments();
        $obj = new CategoryModel();
        $result = $obj->deleteAssignedCat($cat_id[4]);
        if (isset($result) && $result != false) {
            Session::flash('message', 'The assigned category has been deleted successfully'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('admin/categories/assigned-categories');
        }else{
            Session::flash('message', 'Sorry! Something went wrong'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/categories/assigned-categories');
        }
    }
   
}