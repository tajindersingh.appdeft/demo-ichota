<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\admin\UserModel;
use Illuminate\Support\Facades\Hash;
use Session;

/**
 *UsersController class
 * 
 * This class is built to deal with user manage module.
 *
 * @author          Tajinder Singh
 */

class UsersController extends Controller
{	

	/**
     *index function
     * 
     * This function is called to load users listing page.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function index(){
    	$obj = new UserModel();
		$usersDetail = $obj->getUsers();
    	return view('admin.users.users-list', ['results'=> $usersDetail]);
    }

    /**
     *add function
     * 
     * This function is called to add new user in database.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function add(Request $request){

    	$request->validate(["first_name"=> "required","last_name"=> "required","email"=> "required","password"=> "required"]);
		$data =array(
			"first_name" => $request->first_name,
			"last_name"  => $request->last_name,
			"email"      => $request->email,
			"password"   => Hash::make($request->password)
		);
		$obj = new UserModel();
		$save = $obj->add($data);
		if (isset($save) && !empty($save)) {
			Session::flash('message', 'The user has been added successfully'); 
			Session::flash('alert-class', 'alert-success'); 
			return redirect('users');
		}else{
			Session::flash('message', 'Sorry! Something went wrong'); 
			Session::flash('alert-class', 'alert-danger'); 
			return redirect('add-user');
		}
    }
    /**
     * view function
     * 
     * This function is called to view user details.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function view(){
    	/* get user id */
 		$user_id = request()->user_id;
		$obj = new UserModel();
		$results = $obj->view($user_id);
		return view('admin.users.view',['result'=> $results]);
    }



      /**
     * edit function
     * 
     * This function is called to edit user.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function edit(){
    	/* get user id */
 		$user_id = request()->user_id;
		$obj = new UserModel();
		$result = $obj->view($user_id);
		if (isset($result) && !empty($result)) {
			return view('admin.users.edit',compact('result', $result));
		}else{

		}
    }

      /**
     * update function
     * 
     * This function is called to update user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function updateUserDetails(Request $request){
    	
    	$request->validate(["first_name"=>"required", "last_name"=>"required", "password"=>"required"]);
		$obj = new UserModel();
		$data = array(
			"first_name"=>$request->first_name,
			"last_name"=>$request->last_name,
			"password"=> Hash::make($request->password),
		);
		$result = $obj->updateUserDetails($data, $request->user_id);
		if (isset($result) && $result != false) {
            Session::flash('message', 'The user details has been updated successfully'); 
            Session::flash('alert-class', 'alert-success');
			return redirect('users');
		}else{
            Session::flash('message', 'Sorry! Something went wrong'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('users');
		}
    }

      /**
     * deleteUser function
     * 
     * This function is called to delete user details.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function deleteUser(Request $request){
        
        /* get user id */
        $obj = new UserModel();
        $result = $obj->deleteUser($request->user_id);
        if (isset($result) && $result != false) {
            Session::flash('message', 'The user has been deleted successfully'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('users');
        }else{
            Session::flash('message', 'Sorry! Something went wrong'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('users');
        }
    }


    /*******************************  assigned topics report functions  **********************************/

                    /******************** user type 2(curators)  ********************/

    /**
     *userAssignedTopics function
     * 
     * This function is called to get users assined topics.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function userAssignedTopics(){

        $sd = session()->all();
        if (isset($sd['user']) && !empty($sd['user'])) {
            $user_id = $sd['user']['user_id'];
            $obj = new UserModel();
            $result = $obj->getUserAssignedTopics($user_id);

            if (isset($result[0]->first_name) && !empty($result[0]->first_name)) {
                return view('admin.categories.users_topics.user-assigned-topics', compact('result'));
            }else{
                return false;
            }

        }
         return view('page-not-found');
    }

         /**
     *loadAddReport function
     * 
     * This function is called to load add scores to assined topics.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function loadAddReport(){

        $sd = session()->all();
        if (isset($sd['user']) && !empty($sd['user'])) {
            $user_id = $sd['user']['user_id'];
            $obj = new UserModel();
            $result = $obj->getUserAssignedTopics($user_id);

            if (isset($result) && $result == true) {
                return view('admin.categories.users_topics.add-daily-report', compact('result'));
            }else{
                return view('page-not-found');
            }

        }
         return view('page-not-found');
    }



     /**
     *addAssignedTopicsReport function
     * 
     * This function is called to add scores to assined topics.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function addAssignedTopicsReport(Request $request){
        $request->validate([
            "cat_id"=>"required", 
            "score"=>"required", 
            "upbringing"=>"required",
            "influence"=>"required",
            "derived"=>"required",
            "indoctrination"=>"required",
            "afirmation"=>"required",
            "reward"=>"required",
            "promote"=>"required",
            "enforce"=>"required",
            "parroting"=>"required"
        ]);
        $sd = session()->all();
        if (isset($sd['user']) && !empty($sd['user'])) {
            $user_id = $sd['user']['user_id'];
            $data = array(
                'topic_id'=> $request->cat_id,
                'score'=> $request->score,
                'user_id'=> $user_id,
                'upbringing'=> $request->upbringing,
                'influence'=> $request->influence,
                'derived'=> $request->derived,
                'indoctrination'=> $request->indoctrination,
                'afirmation'=> $request->afirmation,
                'reward'=> $request->reward,
                'promote'=> $request->promote,
                'enforce'=> $request->enforce,
                'parroting'=> $request->parroting,
            );
            $obj = new UserModel();
            $result = $obj->addAssignedTopicsReport($data);

            if (isset($result) && $result == true) {
                 Session::flash('message', 'The topic report has been added successfully'); 
                 Session::flash('alert-class', 'alert-success');
           
                return redirect('/admin/topics/assigned-topics');
            }else{
                 Session::flash('message', 'Sorry! Something went wrong'); 
                 Session::flash('alert-class', 'alert-danger'); 
                return redirect('admin.usr.topics');
            }

        }
         return view('page-not-found');
    }



/**
     * getTopicsReports function
     * 
     * This function is called to get logged in user's topic report details.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function getTopicsReports(){
        /* get user id */
        $sd = session()->all();
        if (!isset($sd['user']) && empty($sd['user'])) {
            // return view('page-not-found');
            return redirect('login');
        }elseif (isset($sd['user']['user_id']) && !empty($sd['user']['user_id'])) {
            $user_id = $sd['user']['user_id'];
            $obj = new UserModel();
            if ($sd['user']['user_type'] == 1) {
                $results = $obj->getAllTopicsReports();
            }elseif ($sd['user']['user_type'] != 1) {
                $results = $obj->getTopicsReports($user_id);
            }
            return view('admin.categories.users_topics.list',['results'=> $results]);
        }
    }


}
