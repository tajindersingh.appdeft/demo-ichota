<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\admin\UserModel;
use Illuminate\Support\Facades\Hash;
use Session;

/**
 *LoginController class
 * 
 * This class is built to deal with user login.
 *
 * @author          Tajinder Singh
 */
class LoginController extends Controller
{
	/**
     *index function
     * 
     * This function is called to load login page.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */
   public function index(){
   		$sessionDetails = session()->all();
        if (isset($sessionDetails['user']['email']) && !empty(isset($sessionDetails['user']['email']))) {
            return redirect('home');
        }                                                                
    	return view('admin.login');
    }

	/**
     *checkLogin function
     * 
     * This function is called to check whether a user is authenticated or not to login.
     * 
     * @access          public
     * @param           $request
     * @return          params
     * @author          Tajinder Singh
     */

    public function checkLogin(Request $request){
    	$request->validate(["email"=> "required","password"=> "required"]);
		$data =array(
                "email"      => $request->email,
    			"password"   => $request->password,
		);
		$obj = new UserModel();
		$userDetails = $obj ->getUserDetails($data);
		if (isset($userDetails) && !empty($userDetails) && Hash::check($data['password'], $userDetails->password)) {
				/* save values in session */
				Session::put(
					'user', [
						'first_name' => $userDetails->first_name, 
						'last_name'  => $userDetails->last_name, 
						'email' 	 => $userDetails->email, 
						'user_type'  => $userDetails->user_type,
                        'user_id'    => $userDetails->id
					]
				);
				return redirect('home');
		}else{
			Session::flash('wrong_ep', 'The email/password is wrong!'); 
			Session::flash('alert-class', 'alert-danger'); 
			return redirect('login');
		}
    }

     /**
     *logout function
     * 
     * This function is called to destroy session and logout user.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

     public function logout(){
   		Session::flush();
    	return redirect('login');
    }

}
