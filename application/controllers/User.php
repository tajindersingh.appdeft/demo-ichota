<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Jago FM : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
	
	function podcast()
    {
        $this->global['pageTitle'] = 'Jago FM : podcast';
        
        $this->loadViews("podcast", $this->global, NULL , NULL);
    }
	
	
	function notifications()
    {
        $this->global['pageTitle'] = 'Jago FM : notifications';
        
        $this->loadViews("notifications", $this->global, NULL , NULL);
    }
	
	function signUp()
    {
        $this->global['pageTitle'] = 'Jago FM : signUp';
        
        $this->loadViews("signUp", $this->global, NULL , NULL);
    }
	
	function settings()
    {
        $this->global['pageTitle'] = 'Jago FM : settings';
        
        $this->loadViews("settings", $this->global, NULL , NULL);
    }
	
	function reports()
    {
        $this->global['pageTitle'] = 'Jago FM : reports';
        
        $this->loadViews("reports", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
			$page_type = $this->input->get('type');
			//$this->load->view('users', $page_type);
            $data['searchText'] = $searchText;
            $data['page_type'] = $page_type;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 10 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"],$page_type);
            
            $this->global['pageTitle'] = 'Jago FM : User Listing';
            
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }
	
	
	function category()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->categoryCount($searchText);

			$returns = $this->paginationCompress ( "category/", $count, 10 );
            
            $data['categoryRecords'] = $this->user_model->category($searchText, $returns["page"], $returns["segment"]);
           
            $this->global['pageTitle'] = 'Jago FM : Watch Video';
            
            $this->loadViews("categoryView", $this->global, $data, NULL);
        }
    }
	
	
	function addNewcat()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            
            $this->global['pageTitle'] = 'Jago FM : Add New Category';

            $this->loadViews("addNewcat", $this->global, NULL);
        }
    }
	
	
	function addNewcategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewcat();
            }
            else
            {
                $category_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                
                $userInfo = array('category_name'=> $category_name,'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewcategory($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Category created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category creation failed');
                }
                
                redirect('addNewcat');
            }
        }
    }
	
	
	function video()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->videoCount($searchText);

			$returns = $this->paginationCompress ( "video/", $count, 10 );
            
            $data['videoRecords'] = $this->user_model->video($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Jago FM : Watch Video';
            
            $this->loadViews("videoView", $this->global, $data, NULL);
        }
    }
	
	
	function addNewvid()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            
            $this->global['pageTitle'] = 'Jago FM : Add New Video';
			
			$searchText = '';
           
            $data['categoryRecords'] = $this->user_model->category($searchText, 100,0);

            $this->loadViews("addNewvid", $this->global,$data, NULL);
        }
    }
	
	function addNewvideo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
            $this->form_validation->set_rules('title','Title','trim|required|max_length[256]');
            $this->form_validation->set_rules('url','Url','trim|required|max_length[256]');
            $this->form_validation->set_rules('description','Description','trim|required|max_length[256]');
			$this->form_validation->set_rules('categoryid','Categoryid','trim|required');
			
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewvid();
            }
            else
            {
                $video_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $title = $this->input->post('title');
                $image = $this->input->post('image');
                $url = $this->input->post('url');
                $description = $this->input->post('description');
                $categoryid = $this->input->post('categoryid');
				
				$new_name = time().$_FILES["image"]['name'];
				
					$config['upload_path'] = 'assets/upload/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = 2000;
					$config['max_width'] = 1500;
					$config['max_height'] = 1500;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('image')) 
					{
						$error = array('error' => $this->upload->display_errors());
		
						$this->load->view('addNewvid', $error);
					} 
					else 
					{
						$data = $this->upload->data();
						//print_r($data);die;
						$videoInfo = array('video_name'=> $video_name,'title'=> $title,'image'=> $data['file_name'],'url'=> $url,'description'=> $description,'category_id'=> $categoryid,'createdDtm'=>date('Y-m-d H:i:s'));
                
							$this->load->model('user_model');
							$result = $this->user_model->addNewvideo($videoInfo);
							
							if($result > 0)
							{
								$this->session->set_flashdata('success', 'New Video created successfully');
							}
							else
							{
								$this->session->set_flashdata('error', 'Video creation failed');
							}
						$this->load->view('addNewvid', $data);
					}
				
				redirect('addNewvid');
            }
        }
    }



		function stream_fm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->stream_fmCount($searchText);

			$returns = $this->paginationCompress ( "stream_fm/", $count, 10 );
            
            $data['streamRecords'] = $this->user_model->stream_fm($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Jago FM : Stream FM';
            
            $this->loadViews("stream_view", $this->global, $data, NULL);
        }
    }
	
	
	function addNewstream()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            
            $this->global['pageTitle'] = 'Jago FM : Add New Stream FM';
			
			$searchText = '';

            $this->loadViews("addNewstream", $this->global, NULL);
        }
    }
	
	function addNewstream_fm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
            $this->form_validation->set_rules('url','Url','trim|required|max_length[256]');
			
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewstream();
            }
            else
            {
                $stream_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $url = $this->input->post('url');
				
				
						$streamInfo = array('stream_name'=> $stream_name,'url'=> $url,'createdDtm'=>date('Y-m-d H:i:s'));
                
							$this->load->model('user_model');
							$result = $this->user_model->addNewstream_fm($streamInfo);
							
							if($result > 0)
							{
								$this->session->set_flashdata('success', 'New Stream FM created successfully');
							}
							else
							{
								$this->session->set_flashdata('error', 'Stream FM creation failed');
							}
						$this->load->view('addNewstream', $data);
				
				redirect('addNewstream');
            }
        }
    }

	
	
	function deletestreamfm()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $StreamfmId = $this->input->post('streamfmId');
            $streamfmInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deletestreamfm($StreamfmId, $streamfmInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	

	
	function editOldstreamfm($streamId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($streamId == null)
            {
                redirect('stream_fm');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['streamfmInfo'] = $this->user_model->getstreamfmInfo($streamId);
            
            $this->global['pageTitle'] = 'Jago FM : Stream FM';
            
            $this->loadViews("editOldstreamfm", $this->global, $data, NULL);
        }
    }
	
	
	
	function editstreamfm()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $streamId = $this->input->post('streamId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('url','URL','trim|required|max_length[128]');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldstreamfm($streamId);
            }
            else
            {
                $stream_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $url = $this->input->post('url');
                
                $streamfmInfo = array();
                
                $streamfmInfo = array('stream_name'=>$stream_name,'url'=>$url,'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->editstreamfm($streamfmInfo, $streamId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Stream FM updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Stream FM updation failed');
                }
                
                redirect('stream_fm');
            }
        }
    }
	
	
	
	
	
	
	
	
	function rj()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->rjCount($searchText);

			$returns = $this->paginationCompress ( "rj/", $count, 10 );
            
            $data['rjRecords'] = $this->user_model->rj($searchText, $returns["page"], $returns["segment"]);
           
            $this->global['pageTitle'] = 'Jago FM : RJ and Programs';
            
            $this->loadViews("rjview", $this->global, $data, NULL);
        }
    }
	
	
	function addNewrj()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            
            $this->global['pageTitle'] = 'Jago FM : Add New RJ';

            $this->loadViews("addNewrj", $this->global, NULL);
        }
    }
	
	
	function addNewrjs()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('description','Description','trim|required|max_length[128]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewrj();
            }
            else
            {
                $rj_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $image = $this->input->post('image');
                $description = $this->input->post('description');
				
				$new_name = time().$_FILES["image"]['name'];
				
					$config['upload_path'] = 'assets/upload/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = 2000;
					$config['max_width'] = 1500;
					$config['max_height'] = 1500;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('image'))
					{
						$error = array('error' => $this->upload->display_errors());
		
						$this->load->view('addNewrj', $error);
					} 
					else 
					{
							$data = $this->upload->data();
						
							$rjInfo = array('rj_name'=> $rj_name,'description'=> $description,'image'=> $data['file_name'],'createdDtm'=>date('Y-m-d H:i:s'));
                
							$this->load->model('user_model');
							$result = $this->user_model->addNewrjs($rjInfo);
							
							if($result > 0)
							{
								$this->session->set_flashdata('success', 'New RJ created successfully');
							}
							else
							{
								$this->session->set_flashdata('error', 'RJ creation failed');
							}
						$this->load->view('addNewrj', $data);
					}
				
				redirect('addNewrj');
            }
        }
    }
	
	
	function program()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->programCount($searchText);

			$returns = $this->paginationCompress ( "program/", $count, 10 );
            
            $data['programRecords'] = $this->user_model->program($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Jago FM : Rj and Programs';
            
            $this->loadViews("programview", $this->global, $data, NULL);
        }
    }
	
	
	function addNewpro()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            
            $this->global['pageTitle'] = 'Jago FM : Add New Program';
			
			$searchText = '';
           
            $data['rjRecords'] = $this->user_model->rj($searchText, 100,0);

            $this->loadViews("addNewpro", $this->global,$data, NULL);
        }
    }
	
	
	function addNewprogram()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
            $this->form_validation->set_rules('title','Title','trim|required|max_length[256]');
			$this->form_validation->set_rules('rjid','Rjid','trim|required');
			$this->form_validation->set_rules('datetime','Datetime','trim|required');
			$this->form_validation->set_rules('starttime','Starttime','trim|required');
			$this->form_validation->set_rules('endtime','Endtime','trim|required');
			
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewpro();
            }
            else
            {
                $program_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $title = $this->input->post('title');
                $image = $this->input->post('image');
                $rjid = $this->input->post('rjid');
                $datetime = $this->input->post('datetime');
                $starttime = $this->input->post('starttime');
                $endtime = $this->input->post('endtime');
				
				$new_name = time().$_FILES["image"]['name'];
				
					$config['upload_path'] = 'assets/upload/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = 2000;
					$config['max_width'] = 1500;
					$config['max_height'] = 1500;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('image'))
					{
						$error = array('error' => $this->upload->display_errors());
		
						$this->load->view('addNewpro', $error);
					} 
					else 
					{
						$data = $this->upload->data();
						
						$programInfo = array('program_name'=> $program_name,'title'=> $title,'image'=> $data['file_name'],'rj_id'=> $rjid,'datetime'=> $datetime,'starttime'=> $starttime,'endtime'=> $endtime,'createdDtm'=>date('Y-m-d H:i:s'));
                
							$this->load->model('user_model');
							$result = $this->user_model->addNewprogram($programInfo);
							
							if($result > 0)
							{
								$this->session->set_flashdata('success', 'New Program created successfully');
							}
							else
							{
								$this->session->set_flashdata('error', 'Program creation failed');
							}
						$this->load->view('addNewpro', $data);
					}
				
				redirect('addNewpro');
            }
        }
    }
	
	
	
	
	function ad_management()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->ad_managementCount($searchText);

			$returns = $this->paginationCompress ( "ad_management/", $count, 10 );
            
            $data['ad_managementRecords'] = $this->user_model->ad_management($searchText, $returns["page"], $returns["segment"]);
           
            $this->global['pageTitle'] = 'Jago FM : Ad Management';
            
            $this->loadViews("adview", $this->global, $data, NULL);
        }
    }
	
	function addNewad_manage()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            
            $this->global['pageTitle'] = 'Jago FM : Add New AD Management';

            $this->loadViews("addNewad_manage", $this->global, NULL);
        }
    }
	
	
	function addNewad_management()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
			$this->form_validation->set_rules('price','Price','trim|required');
			$this->form_validation->set_rules('displaydate','Display Date','trim|required');
			$this->form_validation->set_rules('expirydate','Expiry Date','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewad_manage();
            }
            else
            {
                $ad_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
				$image = $this->input->post('image');
                $price = $this->input->post('price');
                $displaydate = $this->input->post('displaydate');
                $expirydate = $this->input->post('expirydate');
				
				$new_name = time().$_FILES["image"]['name'];
                
					$config['upload_path'] = 'assets/upload/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = 2000;
					$config['max_width'] = 1500;
					$config['max_height'] = 1500;
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('image'))
					{
						$error = array('error' => $this->upload->display_errors());
		
						$this->load->view('addNewad_manage', $error);
					} 
					else 
					{
						$data = $this->upload->data();
						//print_r($data);die;
						$adInfo = array('ad_name'=> $ad_name,'ad_image'=> $data['file_name'],'price'=> $price,'display_date'=> $displaydate,'expiry_date'=> $expirydate,'createdDtm'=>date('Y-m-d H:i:s'));
                
						$this->load->model('user_model');
						$result = $this->user_model->addNewad_management($adInfo);
						
						if($result > 0)
						{
							$this->session->set_flashdata('success', 'New AD created successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'AD creation failed');
						}
					
						$this->load->view('addNewad_manage', $data);
					}
				
                redirect('addNewad_manage');
            }
        }
    }
	
	
	
	function regis_userinsight()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->regis_userinsightCount($searchText);

			$returns = $this->paginationCompress ( "regis_userinsight/", $count, 10 );
            
            $data['regis_userinsightRecords'] = $this->user_model->regis_userinsight($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Jago FM : Registered User Insight';
            
            $this->loadViews("regis_userinsight", $this->global, $data, NULL);
        }
    }
	
	
	
	function unregis_userinsight()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
			
            $this->load->library('pagination');
            
            $count = $this->user_model->unregis_userinsightCount($searchText);

			$returns = $this->paginationCompress ( "unregis_userinsight/", $count, 10 );
            
            $data['unregis_userinsightRecords'] = $this->user_model->unregis_userinsight($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Jago FM : Unregistered User Insight';
            
            $this->loadViews("unregis_userinsight", $this->global, $data, NULL);
        }
    }
	
	
	
	
	
	
	
	
	
	
	

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Jago FM : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,
                                    'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'Jago FM : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,
                                    'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 
                        'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }
	
	
	
	function editOldcategory($categoryId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($categoryId == null)
            {
                redirect('category');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['categoryInfo'] = $this->user_model->getcategoryInfo($categoryId);
            
            $this->global['pageTitle'] = 'Jago FM : Edit Category';
            
            $this->loadViews("editOldcategory", $this->global, $data, NULL);
        }
    }
	
	
	function editcategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $categoryId = $this->input->post('categoryId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldcategory($categoryId);
            }
            else
            {
                $category_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                
                $categoryInfo = array();
                
                
                    $categoryInfo = array('category_name'=>ucwords($category_name),'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->editcategory($categoryInfo, $categoryId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Category updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category updation failed');
                }
                
                redirect('category');
            }
        }
    }
	
	
	function editOldvideo($videoId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($videoId == null)
            {
                redirect('video');
            }
            
			$searchText = '';
            $data['categoryRecords'] = $this->user_model->category($searchText,100,0);
			
			
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['videoInfo'] = $this->user_model->getvideoInfo($videoId);
            
            $this->global['pageTitle'] = 'Jago FM : Edit Video';
            
            $this->loadViews("editOldvideo", $this->global, $data, NULL);
        }
    }
	
	
	function editvideo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $videoId = $this->input->post('videoId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
			$this->form_validation->set_rules('title','Title','trim|required|max_length[256]');
            $this->form_validation->set_rules('url','URL','trim|required|max_length[256]');
			$this->form_validation->set_rules('description','Description','trim|required|max_length[256]');
            $this->form_validation->set_rules('categoryid','Categoryid','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldvideo($videoId);
            }
            else
            {
                $video_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $title = $this->input->post('title');
                $image = $this->input->post('image');
                $url = $this->input->post('url');
                $description = $this->input->post('description');
                $categoryid = $this->input->post('categoryid');
				
				//print_r($_FILES['image']['name']);die;
					if($_FILES['image']['name']=="")
					{
						$videoInfo = array();
                
                
						$videoInfo = array('video_name'=>ucwords($video_name), 'title'=>$title, 'url'=>$url,'description'=>$description,'category_id'=>$categoryid,'updatedDtm'=>date('Y-m-d H:i:s'));
					//print_r($videoInfo);die;
						$result = $this->user_model->editvideo($videoInfo, $videoId);
						
						if($result == true)
						{
							$this->session->set_flashdata('success', 'Video updated successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'Video updation failed');
						}
					}
					else
					{
					
						$new_name = time().$_FILES["image"]['name'];
                
							$config['upload_path'] = 'assets/upload/';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size'] = 2000;
							$config['max_width'] = 1500;
							$config['max_height'] = 1500;
							$config['file_name'] = $new_name;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('image')) 
						{
							$error = array('error' => $this->upload->display_errors());
			
							$this->load->view('editOldvideo', $error);
						} 
						else 
						{
							$data = $this->upload->data();
							
							$videoInfo = array();
					
					
							$videoInfo = array('video_name'=>ucwords($video_name), 'title'=>$title,'image'=> $data['file_name'], 'url'=>$url,'description'=>$description,'category_id'=>$categoryid,'updatedDtm'=>date('Y-m-d H:i:s'));
						//print_r($videoInfo);die;
							$old_data=$this->user_model->getvideoInfo($videoId);
							$result = $this->user_model->editvideo($videoInfo, $videoId);
							$imageold = $old_data->image;
							
							if($result == true)
							{
								$this->session->set_flashdata('success', 'Video updated successfully');
								
								unlink(FCPATH.'assets/upload/'.$imageold);
							}
							else
							{
								$this->session->set_flashdata('error', 'Video updation failed');
							}
							
							
						}
					}
				
                redirect('video');
            }
        }
    }
	
	
	
	function editOldrj($rjId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($rjId == null)
            {
                redirect('rj');
            }
            
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['rjInfo'] = $this->user_model->getrjInfo($rjId);
            
            $this->global['pageTitle'] = 'Jago FM : Edit RJ';
            
            $this->loadViews("editOldrj", $this->global, $data, NULL);
        }
    }
	
	
	function editrj()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $rjId = $this->input->post('rjId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldrj($rjId);
            }
            else
            {
                $rj_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $image = $this->input->post('image');
                
				//print_r($_FILES['image']['name']);die;
					if($_FILES['image']['name']=="")
					{
						$rjInfo = array();
                
						$rjInfo = array('rj_name'=>ucwords($rj_name),'updatedDtm'=>date('Y-m-d H:i:s'));
						//print_r($rjInfo);die;
						
						$result = $this->user_model->editrj($rjInfo, $rjId);
						
						if($result == true)
						{
							$this->session->set_flashdata('success', 'RJ updated successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'RJ updation failed');
						}
					}
					else
					{
					
						$new_name = time().$_FILES["image"]['name'];
                
							$config['upload_path'] = 'assets/upload/';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size'] = 2000;
							$config['max_width'] = 1500;
							$config['max_height'] = 1500;
							$config['file_name'] = $new_name;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('image')) 
						{
							$error = array('error' => $this->upload->display_errors());
			
							$this->load->view('editOldrj', $error);
						} 
						else 
						{
							$data = $this->upload->data();
							
							$rjInfo = array();
					
							$rjInfo = array('rj_name'=>ucwords($rj_name),'image'=>$data['file_name'],'updatedDtm'=>date('Y-m-d H:i:s'));
							
							//print_r($rjInfo);die;
							$old_data=$this->user_model->getrjInfo($rjId);
							$result = $this->user_model->editrj($rjInfo, $rjId);
							$imageold = $old_data->image;
							
							if($result == true)
							{
								$this->session->set_flashdata('success', 'RJ updated successfully');
								
								unlink(FCPATH.'assets/upload/'.$imageold);
							}
							else
							{
								$this->session->set_flashdata('error', 'RJ updation failed');
							}
							
							
						}
					}
				
                redirect('rj');
            }
        }
    }
	
	
	function editOldprogram($programId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($programId == null)
            {
                redirect('program');
            }
            
			$searchText = '';
            $data['rjRecords'] = $this->user_model->rj($searchText,100,0);
			
			
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['programInfo'] = $this->user_model->getprogramInfo($programId);
            
            $this->global['pageTitle'] = 'Jago FM : Edit Program';
            
            $this->loadViews("editOldprogram", $this->global, $data, NULL);
        }
    }
	
	
	function editprogram()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $programId = $this->input->post('programId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
			$this->form_validation->set_rules('title','Title','trim|required|max_length[256]');
            $this->form_validation->set_rules('rjid','RJid','trim|required');
			$this->form_validation->set_rules('datetime','Datetime','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldprogram($programId);
            }
            else
            {
                $program_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $title = $this->input->post('title');
                $image = $this->input->post('image');
                $rjid = $this->input->post('rjid');
                $datetime = $this->input->post('datetime');
				
				//print_r($_FILES['image']['name']);die;
					if($_FILES['image']['name']=="")
					{
						$programInfo = array();
                
                
						$programInfo = array('program_name'=>ucwords($program_name), 'title'=>$title, 'rj_id'=>$rjid, 'datetime'=>$datetime,'updatedDtm'=>date('Y-m-d H:i:s'));
					//print_r($programInfo);die;
						$result = $this->user_model->editprogram($programInfo, $programId);
						
						if($result == true)
						{
							$this->session->set_flashdata('success', 'Program updated successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'Program updation failed');
						}
					}
					else
					{
					
						$new_name = time().$_FILES["image"]['name'];
                
							$config['upload_path'] = 'assets/upload/';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size'] = 2000;
							$config['max_width'] = 1500;
							$config['max_height'] = 1500;
							$config['file_name'] = $new_name;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('image')) 
						{
							$error = array('error' => $this->upload->display_errors());
			
							$this->load->view('editOldprogram', $error);
						} 
						else 
						{
							$data = $this->upload->data();
							
							$programInfo = array();
					
					
							$programInfo = array('program_name'=>ucwords($program_name), 'title'=>$title,'image'=> $data['file_name'],'rj_id'=>$rjid,'datetime'=>$datetime,'updatedDtm'=>date('Y-m-d H:i:s'));
						//print_r($programInfo);die;
							$old_data=$this->user_model->getprogramInfo($programId);
							$result = $this->user_model->editprogram($programInfo, $programId);
							$imageold = $old_data->image;
							
							if($result == true)
							{
								$this->session->set_flashdata('success', 'Program updated successfully');
								
								unlink(FCPATH.'assets/upload/'.$imageold);
							}
							else
							{
								$this->session->set_flashdata('error', 'Program updation failed');
							}
							
							
						}
					}
				
                redirect('program');
            }
        }
    }
	
	
	
	function editOldad_management($adId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($adId == null)
            {
                redirect('ad_management');
            }

			
			
            //$data['roles'] = $this->user_model->getUserRoles();
            $data['adInfo'] = $this->user_model->getad_managementInfo($adId);
            
            $this->global['pageTitle'] = 'Jago FM : Edit AD';
            
            $this->loadViews("editOldad_management", $this->global, $data, NULL);
        }
    }
	
	
	
	function editad_management()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $adId = $this->input->post('adId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[256]');
			$this->form_validation->set_rules('price','Price','trim|required');
			$this->form_validation->set_rules('displaydate','Display Date','trim|required');
			$this->form_validation->set_rules('expirydate','Expiry Date','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldad_management($adId);
            }
            else
            {
                $ad_name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
				$image = $this->input->post('image');
                $price = $this->input->post('price');
                $displaydate = $this->input->post('displaydate');
                $expirydate = $this->input->post('expirydate');
				
				//print_r($_FILES['image']['name']);die;
					if($_FILES['image']['name']=="")
					{
						$adInfo = array();
                
                
						$adInfo = array('ad_name'=> $ad_name,'price'=> $price,'display_date'=> $displaydate,'expiry_date'=> $expirydate,'updatedDtm'=>date('Y-m-d H:i:s'));
					//print_r($adInfo);die;
						$result = $this->user_model->editad_management($adInfo, $adId);
						
						if($result == true)
						{
							$this->session->set_flashdata('success', 'AD updated successfully');
						}
						else
						{
							$this->session->set_flashdata('error', 'AD updation failed');
						}
					}
					else
					{
					
						$new_name = time().$_FILES["image"]['name'];
                
							$config['upload_path'] = 'assets/upload/';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size'] = 2000;
							$config['max_width'] = 1500;
							$config['max_height'] = 1500;
							$config['file_name'] = $new_name;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('image')) 
						{
							$error = array('error' => $this->upload->display_errors());
			
							$this->load->view('editOldad_management', $error);
						} 
						else 
						{
							$data = $this->upload->data();
							
							$adInfo = array();
					
					
							$adInfo = array('ad_name'=> $ad_name,'ad_image'=> $data['file_name'],'price'=> $price,'display_date'=> $displaydate,'expiry_date'=> $expirydate,'updatedDtm'=>date('Y-m-d H:i:s'));
						//print_r($adInfo);die;
							$old_data=$this->user_model->getad_managementInfo($adId);
							$result = $this->user_model->editad_management($adInfo, $adId);
							$imageold = $old_data->ad_image;
							
							if($result == true)
							{
								$this->session->set_flashdata('success', 'AD updated successfully');
								
								unlink(FCPATH.'assets/upload/'.$imageold);
							}
							else
							{
								$this->session->set_flashdata('error', 'AD updation failed');
							}
							
							
						}
					}
				
                redirect('ad_management');
            }
        }
    }
	
	
	
	


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	function deleteCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $categoryId = $this->input->post('categoryId');
            $categoryInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteCategory($categoryId, $categoryInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function deleteVideo()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $videoId = $this->input->post('videoId');
            $videoInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteVideo($videoId, $videoInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function deleteRj()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $rjId = $this->input->post('rjId');
            $rjInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteRj($rjId, $rjInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function deleteprogram()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $programId = $this->input->post('programId');
            $programInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteprogram($programId, $programInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function deletead_management()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $adId = $this->input->post('adId');
            $adInfo = array('isDeleted'=>1,'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deletead_management($adId, $adInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	
	
    
	
	function isactivate()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
			$action = $this->input->post('action');
			$status = $this->input->post('status');
			if($action =="request-page")
			{
				$userInfo = array('approve_reject'=>$status,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
			}
			else
			{
				$userInfo = array('isactive'=>$status,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
			}
			
			
            
            $result = $this->user_model->isactivate($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function isactivatecategory()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $categoryId = $this->input->post('categoryId');
			$status = $this->input->post('status');
			
			$categoryInfo = array('isactive'=>$status,'updatedDtm'=>date('Y-m-d H:i:s'));
			
            $result = $this->user_model->isactivatecategory($categoryId, $categoryInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function isactivatevideo()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $videoId = $this->input->post('videoId');
			$status = $this->input->post('status');
			
			$videoInfo = array('isactive'=>$status,'updatedDtm'=>date('Y-m-d H:i:s'));
			
            $result = $this->user_model->isactivatevideo($videoId, $videoInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function isactivaterj()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $rjId = $this->input->post('rjId');
			$status = $this->input->post('status');
			
			$rjInfo = array('isactive'=>$status,'updatedDtm'=>date('Y-m-d H:i:s'));
			
            $result = $this->user_model->isactivaterj($rjId, $rjInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	function isactivateprogram()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $programId = $this->input->post('programId');
			$status = $this->input->post('status');
			
			$programInfo = array('isactive'=>$status,'updatedDtm'=>date('Y-m-d H:i:s'));
			
            $result = $this->user_model->isactivateprogram($programId, $programInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	
	
	
	
	
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Jago FM : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Jago FM : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'Jago FM : My Profile' : 'Jago FM : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]|callback_emailExists');        
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            
            $userInfo = array('name'=>$name, 'email'=>$email, 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->editUser($userInfo, $this->vendorId);
            
            if($result == true)
            {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }

            redirect('profile/'.$active);
        }
    }

    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect('profile/'.$active);
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     * @param {string} $email : This is users email
     */
    function emailExists($email)
    {
        $userId = $this->vendorId;
        $return = false;

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ $return = true; }
        else {
            $this->form_validation->set_message('emailExists', 'The {field} already taken');
            $return = false;
        }

        return $return;
    }
	
}

?>