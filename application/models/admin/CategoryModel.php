<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class CategoryModel extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name' ];    



     /**
     * add function
     * 
     * This function is called to add new user in database.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function add($data){
    	DB::table('categories')->insert($data);
    	return true;
    }

     /**
     * updateCat function
     * 
     * This function is called to update category in database.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function updateCat($data, $id){
	
        $result = DB::table('categories')->where('id', $id)->limit(1)->update($data);
            if (is_null($result)) {
                return false;
            }else{
                return true;
            }
    }


        /**
     * deleteCat function
     * 
     * This function is called to delete category from database.
     * 
     * @access          public
     * @param           $id
     * @return          params
     * @author          Tajinder Singh
     */

    public function deleteCat($id){
        $data = array("deleted_at"=> date("Y-m-d H:i:s"));
        $result = DB::table('categories')->where('id', $id)->limit(1)->update($data);
        return true;
    }


    /**
     * assignCatToUsers function
     * 
     * This function is called to assign category to user.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function assignCatToUsers($data){
    	DB::table('users_assigned_categories')->insert($data);
    	return true;
    }


      /**
     *  getAssignedCat function
     * 
     * This function is called to delete user from database.
     * 
     * @access          public
     * @param           $id
     * @return          params
     * @author          Tajinder Singh
     */

    public function  getAssignedCat(){



        $result = DB::table('users_assigned_categories') 
            ->join('users', 'users_assigned_categories.user_id', '=', 'users.id')
            ->join('categories', 'users_assigned_categories.category_id', '=', 'categories.id')
            ->select('users_assigned_categories.id', 'users.id as user_id', 'users.first_name', 'users.last_name', 'categories.name', 'users_assigned_categories.created_at')->where('users_assigned_categories.deleted_at', NULL)->get();
        return $result;

            // if (is_null($result)){return false;}
            // else{return true;}
    }


      /**
     *  checkAssignedCat function
     * 
     * This function is called to check if category is already assigned to user.
     * 
     * @access          public
     * @param           $data
     * @return          params
     * @author          Tajinder Singh
     */

    public function  checkAssignedCat($data){

        $result = DB::table('users_assigned_categories')->where('user_id', $data['user_id'])->where('category_id', $data['category_id'])->where('deleted_at', NULL)->first();
        if (!empty($result)) {
            return true;
        }else{
            return false;
        }
    }


    /**
     * deleteAssignedCat function
     * 
     * This function is called to delete user from database.
     * 
     * @access          public
     * @param           $id
     * @return          params
     * @author          Tajinder Singh
     */

    public function deleteAssignedCat($id){

        $data = array("deleted_at"=> date("Y-m-d H:i:s"));
        $result = DB::table('users_assigned_categories')->where('id', $id)->limit(1)->update($data);
        return true;
    }



}
