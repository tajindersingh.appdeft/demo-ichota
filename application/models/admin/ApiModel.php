<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ApiModel extends Model
{
    use HasFactory;

    
     /**
     * getUsersDailyReport function
     * 
     * This function is called get topics report of all contributors.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

     public function getUsersDailyReport(){
         $res =  DB::table('users.first_name, users.last_name, categories.name as topic_name, topics_reports.*')
                    ->join('users', 'topics_reports.user_id', '=', 'users.id')
                    ->join('categories', 'topics_reports.topic_id', '=', 'categories.id')
                    ->where('deleted_at', NULL)->get();

                    print_r($res); die;

     }
}
