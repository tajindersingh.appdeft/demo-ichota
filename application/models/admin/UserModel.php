<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model
{
    use HasFactory;

    protected $table = 'users';


     /**
     * getUserDetails function
     * 
     * This function is called to get user details from database.
     * 
     * @access          public
     * @param           $data
     * @return          params
     * @author          Tajinder Singh
     */

    public function getUserDetails($data){

        $userDetails = DB::table('users')->where('email', $data['email'])->where('deleted_at', NULL)->first();
    	return $userDetails;
    }

    /**
     * getUsers function
     * 
     * This function is called to get all user details from database.
     * 
     * @access          public
     * @param           $data
     * @return          params
     * @author          Tajinder Singh
     */

    public function getUsers(){
    	return DB::table('users')->where('deleted_at', NULL)->get();
    }


    /**
     * add function
     * 
     * This function is called to add new user in database.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function add($data){
    	DB::table('users')->insert($data);
    	return true;
    }


    /**
     * view function
     * 
     * This function is called to view user details.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function view($user_id){
    	return DB::table('users')->where('id', $user_id)->first();
    }

     /**
     * updateUserDetails function
     * 
     * This function is called to update user details in database.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function updateUserDetails($data,$user_id){
	
        $UpdateDetails = DB::table('users')->where('id', $user_id)->limit(1)->update($data);
            if (is_null($UpdateDetails)) {
                return false;
            }else{
                return true;
            }
    }

    /**
     * deleteUser function
     * 
     * This function is called to delete user from database.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          Tajinder Singh
     */

    public function deleteUser($user_id){
        $data = array("deleted_at"=> date("Y-m-d H:i:s"));
        $UpdateDetails = DB::table('users')->where('id', $user_id)->limit(1)->update($data);
            
        return true;
            // if (is_null($UpdateDetails)){return false;}
            // else{return true;}
    }


    /***************** user type 2(normal user) ******************/





    /**
     * getUserAssignedTopics
     * 
     * This function is called to get user's assigned topics from database.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          Tajinder Singh
     */

    public function getUserAssignedTopics($user_id){
        $result = DB::table('users_assigned_categories')
            ->join('users', 'users_assigned_categories.user_id', '=', 'users.id')
            ->join('categories', 'users_assigned_categories.category_id', '=', 'categories.id')
            ->select('users.id as user_id', 'categories.id as cat_id', 'users.first_name', 'users.last_name', 'categories.name as cat_name')->where('users_assigned_categories.user_id', $user_id)->get();
        if (is_null($result[0]->first_name)){return false;}
        else{return $result;}
    }

     /**
     * addAssignedTopicsReport function
     * 
     * This function is called to add daily assigned topics report.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          Tajinder Singh
     */

    public function addAssignedTopicsReport($data){
        DB::table('topics_reports')->insert($data);
        return true;
    }

    /**
     * getTopicsReports function
     * 
     * This function is called to get logged in user details from database.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          Tajinder Singh
     */

    public function getTopicsReports($user_id){

    $result = DB::table('topics_reports')
    ->join('users', 'topics_reports.user_id', '=', 'users.id')
    ->join('categories', 'topics_reports.topic_id', '=', 'categories.id')
    ->select('topics_reports.*', 'users.first_name', 'users.last_name', 'categories.name as topic_name')
    ->where('topics_reports.user_id', $user_id)
    ->where('topics_reports.deleted_at', NULL)->get();
        return $result;
    }


    /**
     * getAllTopicsReports function
     * 
     * This function is called to get all user details from database.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          Tajinder Singh
     */

    public function getAllTopicsReports(){

    $result = DB::table('topics_reports')
    ->join('users', 'topics_reports.user_id', '=', 'users.id')
    ->join('categories', 'topics_reports.topic_id', '=', 'categories.id')
    ->select('topics_reports.*', 'users.first_name', 'users.last_name', 'categories.name as topic_name')
    ->where('topics_reports.deleted_at', NULL)->get();
        return $result;
    }



}
