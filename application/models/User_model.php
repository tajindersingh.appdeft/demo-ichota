<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
use Twilio\Rest\Client;
/**
 * Class : User_model (User Model)
 * User model class to get to handle user related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.email,BaseTbl.isactive, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '')
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.email,BaseTbl.isactive, BaseTbl.approve_reject, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        //$this->db->order_by('BaseTbl.userId', 'DESC');
        //$this->db->limit($segment, $page);
        $query = $this->db->get();
        
        $result = $query->result();       
        return $result;
    }
    
    
    /* function userListingbyuserid($userId)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.email,BaseTbl.isactive, BaseTbl.approve_reject, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        $this->db->where('BaseTbl.userId', $userId);
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        $result = $query->result();       
        return $result;
    } */
    
    function userListingbyuserid($userId)
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.email,BaseTbl.isactive, BaseTbl.approve_reject, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.roleId');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        $this->db->where('BaseTbl.user_id', $userId);
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        $result = $query->result();       
        return $result;
    }
    
    /* function userListingbyuserid($userId)
    {
        $this->db->select('userId,email,isactive,approve_reject,name,mobile,createdDtm,roleId');
        $this->db->from('tbl_users');
       // $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        $result = $query->result();       
        return $result;
    } */
    
    
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select('*');
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("user_id !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
	function checkplatformgmail($gmail_id)
	{
		$this->db->select('*');
        $this->db->from("tbl_users");
        $this->db->where("gmail_id", $gmail_id);   
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();

        return $query->row();
	}
	function checkplatformfacebook($facebook_id)
	{
		$this->db->select('*');
        $this->db->from("tbl_users");
        $this->db->where("facebook_id", $facebook_id);   
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();

        return $query->row();
	}
	function checkEmailExists_new($name, $mobile,$profile_id, $userId=0)
    {
        $this->db->select('*');
        $this->db->from("tbl_users");
        $this->db->where("name", $name);   
        $this->db->where("mobile", $mobile);   
        $this->db->where("device_id", $profile_id);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("user_id !=", $userId);
        }
        $query = $this->db->get();

        return $query->row();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
       
        $this->db->insert('tbl_users', $userInfo);        
        $insert_id = $this->db->insert_id();    
        
        $this->db->select(["email","mobile","password","user_id","device_id"]);
        $this->db->from("tbl_users");          
        $this->db->where("isDeleted", 0);        
        $this->db->where("user_id", $insert_id); 
        $query = $this->db->get();
        $result = $query->result();
        return  $result;
    }
    
    /**
     * This function is used to add new quote to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewlist($listInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_listings', $listInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /*
    * This function is used to view quotes
    */
    function viewlist($userId)
    {
        $this->db->select('*');
        $this->db->from('tbl_listings');
        $this->db->where("isDeleted", 0);
        if(!empty($userId))
        {
            $this->db->where('user_id', $userId);
        }
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    
    /*
    * This function is used to view quotes by quoteid
    */
    function viewlist_bylistid($list_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_listings');
        $this->db->where("isDeleted", 0);
        $this->db->where('list_id', $list_id);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('user_id, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('roleId !=', 1);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {

        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return TRUE;
    }
    function editpost($postInfo, $postId)
    {
        $this->db->where('id', $postId);
        $this->db->update('tbl_post_item', $postInfo);
        
        return TRUE;
    }
    
    /**
     * This function is used to update the list information
     * @param array $listInfo : This is list updated information
     * @param number $list_id : This is list id
     */
    function editlist($catInfo, $list_id)
    {
        $this->db->where('list_id', $list_id);
        $this->db->update('tbl_listings', $catInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }
    
    /**
     * This function is used to delete the list information
     * @param number $list_id : This is list id
     * @return boolean $result : TRUE / FALSE
     */
    function deletelist($list_id, $listInfo)
    {
        $this->db->where('list_id', $list_id);
        $this->db->update('tbl_listings', $listInfo);
        
        return $this->db->affected_rows();
    }
    
    
    
    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('user_id, password');
        $this->db->where('user_id', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to get user login history
     * @param number $userId : This is user id
     */
    function loginHistoryCount($userId, $searchText, $fromDate, $toDate)
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.user_id', $userId);
        }
        $this->db->from('tbl_last_login as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    /**
     * This function is used to get user login history
     * @param number $userId : This is user id
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function loginHistory($userId, $searchText, $fromDate, $toDate, $page, $segment)
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        $this->db->from('tbl_last_login as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.user_id', $userId);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfoById($userId)
    {
        $this->db->select('user_id, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }

    /**
     * This function used to get user information by id with role
     * @param number $userId : This is user id
     * @return aray $result : This is user information
     */
    function getUserInfoWithRole($userId)
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.roleId, Roles.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Roles','Roles.roleId = BaseTbl.roleId');
        $this->db->where('BaseTbl.user_id', $userId);
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    
    function isactivate($userId, $userInfo)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }
    
    
    
    public function login_api($email,$password){
        
        $this->db->select('user_id, email, name, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    
    function viewlistuser()
    {
        $this->db->select('user_id,name');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
       return $query->result_array();
    }
    
    public function addmanagecat($catInfo){
        $this->db->trans_start();
        $this->db->insert('tbl_category', $catInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    public function addmanageservicecat($catserInfo){
        $this->db->trans_start();
        $this->db->insert('tbl_services_category', $catserInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function catlist($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where("isDeleted", 0);
        if(!empty($category_id))
        {
            $this->db->where('category_id', $category_id);
        }
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    } 
    
    function servicelist($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_services_category');
        $this->db->where("isDeleted", 0);
        if(!empty($id))
        {
            $this->db->where('service_id', $id);
        }
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    function catlist_bycatid($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where("isDeleted", 0);
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    public function sercatlist($service_id) {
        $this->db->select('*');
        $this->db->from('tbl_services_category');
        if(!empty($service_id)) {
            $this->db->where('id', $service_id);
        }
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    
    function editcat($catInfo, $category_id)
    {
        $this->db->where('category_id', $category_id);
        $this->db->update('tbl_category', $catInfo);
        
        return TRUE;
    }
    
    function deletecat($category_id, $catInfo)
    {
        $this->db->where('category_id', $category_id);
        $this->db->update('tbl_category', $catInfo);
        
        return $this->db->affected_rows();
    }
    
    public function addcontentsec($cntsecInfo){
        $this->db->trans_start();
        $this->db->insert('tbl_content_section', $cntsecInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function contentseclist($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_content_section');
        $this->db->where("isDeleted", 0);
        if(!empty($id))
        {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    function contentseclist_bycontentid($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_content_section');
        $this->db->where("isDeleted", 0);
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    function editcontentsec($cntsecInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_content_section', $cntsecInfo);
        
        return TRUE;
    }
    
    function deletecontsec($id, $cntsecInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_content_section', $cntsecInfo);
        
        return $this->db->affected_rows();
    }
    
    function totaluseractivelist()
    {
        $this->db->select('count(isactive) as Active');
        $this->db->from('tbl_users');
        //$this->db->where('isDeleted', 0);
        $this->db->where('isactive =', 1);
        
        $query = $this->db->get();
        
       return $query->result_array();
    }
    
    function totaluserinactivelist()
    {
        $this->db->select('count(isactive) as Inactive');
        $this->db->from('tbl_users');
        //$this->db->where('isDeleted', 0);
        $this->db->where('isactive =', 0);
        
        $query = $this->db->get();
        
       return $query->result_array();
    }
    
    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
     function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }
    function add_post_item($postInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_post_item', $postInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    function add_service_category($serviceInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_service', $serviceInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    /* function add_post_item()
    {
         $this->db->trans_begin();

            $userInfo = array(
                'user_id'          => $this->getUserId(),     //UserId fetch from session
                'title'            => $this->getTitle(),
                'description'      => $this->getDescription(),
            );

            $this->db->insert('UserInfo', $userInfo);   //Data inserted to table UserInfo first

            $insert_id = $this->db->insert_id();        //And getting the inserted_id

            $data[] = array(
                'user_id'          => $this->getUserId(),
                'email'         => $insert_id,            //Insert Inserted id
                'image_name'       => $this->getImage(),
            );

            $this->db->insert_batch('UserImage', $data);     //Insert data to table UserImage

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
                return ($this->db->affected_rows() != 1) ? false : true;
            }
    } */
    function category_name_list()
    {
        $this->db->select('id,name');
        $this->db->from('tbl_category');
        $this->db->where("isDeleted", 0);
        $query = $this->db->get(); 
        $result = $query->result();        
        return $result;
    }
    
    function search_category($searchtext)
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $likeCriteria = "(name  LIKE '%".$searchtext."%')";
        $this->db->where($likeCriteria);  
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();   
        $result = $query->result();    
        return $result;
    }
    /*function search_postitem($searchtext)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $likeCriteria = "(product_name  LIKE '%".$searchtext."%'
                           OR  product_description  LIKE '%".$searchtext."%'
                           OR  product_price  LIKE '%".$searchtext."%'
                           OR  Buying_option  LIKE '%".$searchtext."%'
                           OR  select_box_size  LIKE '%".$searchtext."%'
                           OR  distance  LIKE '%".$searchtext."%')";
        $this->db->where($likeCriteria);   
        $this->db->where('isDeleted', 0);
        $query = $this->db->get(); 
        $result1 = $query->result();    
        return $result1;
    }*/



    /* Search post item using lat, long and distance */
    function search_postitem($searchtext,$lat,$long,$miles) {

        $query = $this->db->query("SELECT *, (3959 * acos(cos(radians($lat)) * cos(radians(latitude)) * cos( radians(longitude) - radians($long)) + sin(radians($lat)) * sin(radians(latitude)))) AS distance FROM `tbl_post_item` WHERE `product_name`  LIKE '%".$searchtext."%' OR  `product_description`  LIKE '%".$searchtext."%'  OR  `product_price`  LIKE '%".$searchtext."%' OR  `Buying_option`  LIKE '%".$searchtext."%' OR  `select_box_size`  LIKE '%".$searchtext."%' OR  `distance`  LIKE '%".$searchtext."%' HAVING distance < $miles ORDER BY distance");
        return $query->result();

    }
    /* Search post item ends here */


    function search_item_filter($minimum_product_price,$maximum_product_price,$Buying_option,$conditions,$Distance)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        if(!empty($maximum_product_price) && !empty($minimum_product_price))
        {
            //$this->db->where('product_price =',$product_price);
            $this->db->where('product_price <=',$maximum_product_price);
            $this->db->where('product_price >=',$minimum_product_price);
        }
        if(!empty($Buying_option))
        {
            $this->db->where('Buying_option =',$Buying_option);
        }
        if(!empty($conditions))
        {
            $this->db->where('conditions =',$conditions);
        }
        if(!empty($Distance))
        {
            $this->db->where('distance =',$Distance);
        }
        $this->db->where('isDeleted', 0);


        $query = $this->db->get();
        return $query1 = $this->db->last_query();
       /* $result = $query->result();  
        
        return $result;*/
    }
    
    
    
    function check_add_to_notification($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_notification');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
    }
    
   
    function add_notification($resultdata)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_notification', $resultdata);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

   
    
    /*  function update_favourite($resultdata, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_add_to_favourite', $resultdata);
        //print_r($this);
        return TRUE;
    }*/
    
    function update_notification($resultdata, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_notification', $resultdata);
        //print_r($this);
        return TRUE;
    }

    
    function view_notification($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_notification');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();    
        $result = $query->result();  
        return $result;
    }
    
    function viewprodcategory($myArraydata)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        if(!empty($myArraydata))
        {
            $this->db->where_in("id", $myArraydata);
        }
        
        $this->db->where("isDeleted", 0);
        //$this->db->where("isactive", 1);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    
    function viewusercategory($myArraydata)
    {
        $this->db->select('user_id,name,user_image,description');
        $this->db->from('tbl_users');
        if(!empty($myArraydata))
        {
            $this->db->where_in("user_id", $myArraydata);
        }
        
        $this->db->where("isDeleted", 0);
        //$this->db->where("isactive", 1);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    
    function viewpost_item()
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();   
       return $query->result_array();
    }
    
    function viewpost_bynewestid($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where("isDeleted", 0);
        $this->db->where("category_id", $category_id);
        $this->db->order_by('id', 'DESC');        
        $query = $this->db->get();
        return $query->result_array();
    }


    
    function viewservice_bynewestid()
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where("isDeleted", 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function viewpost_byhightolow()
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where("isDeleted", 0);
        //$this->db->group_by('product_price');
        $this->db->order_by('product_price', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    /* function viewpost_byhightolow()
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where("isDeleted", 0);
        //$this->db->group_by('product_price');
        $this->db->order_by('product_price', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    } */
    
    function viewpost_bylowtohigh()
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where("isDeleted", 0);
        //$this->db->group_by('product_price');
        $this->db->order_by('product_price', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function viewproduct_by_product_id($product_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where('id', $product_id);
        $this->db->where('isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();   
        $result = $query->result();       
        return $result;
    }
    
    function viewservice_by_service_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where('id', $user_id);
        $this->db->where('isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();   
        $result = $query->result();       
        return $result;
    }
        
    function viewproduct_by_category ($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where('category_id', $category_id);
        $this->db->where('isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get(); 
        $result = $query->result();       
        return $result;
    }

    function user_follower($userfollower,$user_id)
    {
         $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users', $userfollower);  
        return TRUE;
    }
    function user_following($userfollowing,$user_id)
    {
         $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users', $userfollowing);   
        return TRUE;
    }
    function viewpost_bycategoreyid($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where("isDeleted", 0);
        $this->db->where("active_sold_status", 0);
        $this->db->where('category_id', $category_id);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    
    function viewpost_byid($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where("isDeleted", 0);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    function viewservice_bycategoreyid($service_categoryid)
    {
        $this->db->select('*');
        $this->db->from('tbl_service_item');
        
        $this->db->where('service_category_id', $service_categoryid);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    /* function check_follower_following_userid($current_userId,$Follower_userId)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('Followers', $Follower_userId);
        $this->db->where('Following', $current_userId);
        $query = $this->db->get();
        
        return $query->row();
    }  */
    function check_add_status($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_followers');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();  
        return $query->row();
    }

    function update_followstatus($resultdata,$follower_id)
    {
        $this->db->where('user_id', $follower_id);
        $this->db->update('tbl_followers', $resultdata);
        //print_r($this);
        return TRUE;
    }
    /* function geo_location($latitude,$longitude)
    {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where("isDeleted", 0);
        $this->db->where('latitude', $latitude);
        $this->db->where('longitude', $longitude);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    } */
    function otp_varification($data)
    {
        $result = $this->db->insert('tbl_users', $data);
        if($result) {
            return TRUE;
        } 
        else {
                return FALSE;
        }
    }
    
    function viewuser_by_id ($userId)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('user_id', $userId);
        $this->db->where('isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get(); 
        $result = $query->result();       
        return $result;
    }
    
    function viewlistuserbyid($userId)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('roleId !=', 1);
        $this->db->where('user_id',$userId);
        $query = $this->db->get();    
        $result = $query->result();        
        return $result;
    }



    function getPostsOfUser($userId) {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where('isDeleted', 0);
        $this->db->where('user_id',$userId);
        $query = $this->db->get();   
        $result = $query->result();        
        return $result;
    }
    
    function geo_location_distance($lat,$long)
    {
        $query = $this->db->query("SELECT *, (3959 * acos(cos(radians($lat)) * cos(radians(latitude)) * cos( radians(longitude) - radians($long)) + sin(radians($lat)) * sin(radians(latitude)))) AS distance FROM tbl_post_item HAVING distance < 100 ORDER BY distance LIMIT 0 , 10");
        return $query->result();        
    }

       //..........follwers and follwing api function    start
      /*
     function common($id)
    {
      
        $this->db->select ('tbl_followers.follower_id);
        $this->db->from("tbl_followers");
        $this->db->where('user_id', $id);
        $query = $this->db->get();
       
      $data = array();
       foreach ($query->result() as $row)
      {
       $data[] = array(' total follower' => count( unserialize($row->follower_id)), ' total following' => count(unserialize($row->following_id))); 
     }
          echo "<pre>";
       return json_encode($data);
         echo "</pre>";

    }   */

    //next function...............
    // function add_followstatus($resultdata)
    // {        
    //     $this->db->trans_start();
    //     $this->db->insert('tbl_followers', $resultdata);
    //     $insert_id = $this->db->insert_id();
    //     $this->db->trans_complete();

    //     /* updated user table to display followers and followings users */
    //     $this->db->select ('*');
    //     $this->db->from("tbl_followers");
    //     $this->db->where('user_id', $resultdata['user_id']);
    //     $this->db->where('follower_id !=', 0);
    //      $this->db->where('status', 1);
    //     $query = $this->db->get();
    //     $total_followers = count($query->result());        


    //     $this->db->select ('*');
    //     $this->db->from("tbl_followers");
    //     $this->db->where('user_id', $resultdata['user_id']);
    //     $this->db->where('following_id !=', 0);
    //      $this->db->where('status', 1);
    //     $query = $this->db->get();
    //     $total_following = count($query->result());        
        
    //     $userInfo = array('follower_count' => $total_followers,
    //                       'following_count' => $total_following );

    //     $this->db->where('user_id', $resultdata['user_id']);
    //     $this->db->update('tbl_users', $userInfo); 
    //     return TRUE;

    //     // return $insert_id;
    // }

    function tbl($id)
    {   
        $this->db->select ('*');
        $this->db->from("tbl_followers");
        $this->db->where('user_id', $id);
        $this->db->where('follower_id !=', 0);
         $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();            
    }  

    function myfollower($followersids){     
       
        $query = $this->db->query("select * from tbl_users where user_id in (".$followersids.")");
        return $query->result();     
    }

    /**
     *getfollowedUsersByMe function
     * 
     * This function is called to get users i followed.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          
     */

    function getfollowedUsersByMe($id)
    {
        $this->db->select('tbl_users.*');
        $this->db->from("tbl_followers");
        $this->db->join('tbl_users','tbl_followers.following_id = tbl_users.user_id');
        $this->db->where('tbl_followers.user_id', $id);
         $this->db->where('tbl_followers.status', '1');
        $query = $this->db->get();
        return $query->result_array();
    }  



    /**
     *getUsersfollowingMe function
     * 
     * This function is called to get users who following me.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          
     */

    function getUsersfollowingMe($id)
    {
        $this->db->select('tbl_users.*');
        $this->db->from("tbl_followers");
        $this->db->join('tbl_users','tbl_followers.user_id = tbl_users.user_id');
        $this->db->where('tbl_followers.following_id', $id);
        $this->db->where('tbl_followers.status', '1');
        $query = $this->db->get();
        return $query->result_array();
    }  

    //rating function....................

    function rating2($id)
    {
        $this->db->select('tbl_users.name,tbl_users.rating,tbl_users.createdDtm,tbl_users.comment');
        $this->db->from("tbl_users");
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $result = $query->result();       
        return $result;
    }

    // user_image function.....................

    /* function userimage($id,$data)
    {
        $result=$this->db->where("userId", $id);
        $this->db->update("tbl_users", $data);
        if($result){return "update";}
        else{return "not update";}
    } */

    function userimage($userInfo, $userId)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo); 
        return TRUE;
    }

     //........................................................................


     function total_followers($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_followers');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        $result = $query->result();   
        return $result;
    }

    //......remove follower  then update  function  ..................................................................

      function testt()
    {
        $this->db->select ('*');
        $this->db->from("tbl_followers");
       // $this->db->where('user_id',
           // $id);
        $query = $this->db->get();
         return $query->result();  
    }  


/* extra function */
      function removefollower($id)
    {
        $this->db->select ('tbl_followers.follower_id');
        $this->db->from("tbl_followers");
        $this->db->where('user_id', $id);
        $query = $this->db->get();
         return $query->result();  
    }
/* end extra function */

    function checkMyFollwer($user_id, $follower_id) {
        $this->db->select ('user_id as follower_id');
        $this->db->from("tbl_followers");
        $this->db->where('following_id', $user_id);
        $this->db->where('user_id', $follower_id);
        $query = $this->db->get();
         return $query->result();  
    }


    function checkMyFollwing($user_id, $following_id) {
        $this->db->select ('following_id');
        $this->db->from("tbl_followers");
        $this->db->where('following_id', $following_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
         return $query->result();  
    }


    function removeMyFollower($user_id, $follower_id) {                               
        $this->db->delete('tbl_followers', array('following_id' => $user_id, 'user_id' => $follower_id));
         return true;
        }

    function removeUsersIAmfollowing($user_id, $following_id) {                               
        $this->db->delete('tbl_followers', array('user_id' => $user_id, 'following_id' => $following_id));
         return true;
        }


//........ Remove data favorites list................................................................................
                //.....delete tbl services.............
    function check_id_exist2($id)
    {
        $this->db->select ('*');
            $this->db->from("tbl_service");
            $this->db->where('id',$id);
             $query = $this->db->get();
            $result = $query->result();        
            return $result;
    }


    function delete_service_data($id)
     {                            
            
         $this->db->where('id', $id);
         $this ->db-> delete('tbl_service');
         return TRUE;                 
               
    }  



 //.........................................
    // delete tbl post item................................
    function check_id_exist($id)
    {
        $this->db->select ('*');
            $this->db->from("tbl_post_item");
            $this->db->where('id',$id);
             $query = $this->db->get();
            $result = $query->result();        
            return $result;
    }


    function delete_product_data($id)
    {                            
         $this->db->where('id', $id);
         $this ->db-> delete('tbl_post_item');
         return TRUE;                 
               
    }

    function check_id_exist3($category_id)
    {
         $this->db->select ('*');
         $this->db->from("tbl_category");
         $this->db->where('category_id',$category_id);
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }   
    
    function delete_post_categorey_data($category_id)
    {                            
         $this->db->where('category_id', $category_id);
         $this ->db-> delete('tbl_category');
         return TRUE;                 
               
    }   

//.............................................................


    function product_id($id)
    {                            
            $this->db->select ('tbl_add_to_favourite.product_id');
            $this->db->from("tbl_add_to_favourite");
            $this->db->where('user_id', $id);
            $query = $this->db->get();
            return $query->result();   
    }  
    
    function remove_product_id($id,$data)
    {                       
        $re=$this->db->where("user_id=", $id);
        $this->db->update("tbl_add_to_favourite", $data);   
    }
//.............active_ads  and sold_ads function..........................................................


    function active_data($id) {
        $this->db->select ('*');
        // $this->db->select ('CONCAT(image_path, \' \', product_cover_img)');
        $this->db->from("tbl_post_item");
        $this->db->where('user_id', $id);
        $this->db->where('active_sold_status', 0);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();
        return $query->result(); 
    }

    function sold_data($id)
    {
        $this->db->select ('*');
        $this->db->from("tbl_post_item");
        $this->db->where('user_id', $id);
        $this->db->where('active_sold_status', 1);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();
        return $query->result(); 
   } 

//................................................................................................................

// ..................................all data  functions  here...........

    
    function combine_data($user_id)
    {
        $this->db->select('Roles.user_id,Roles.name,Roles.user_image,Roles.latitude,Roles.longitude,Roles.rating,
            Roles.description, Roles.comment,Roles.Email_status,Roles.instagram_status,Roles.phone_no_status,
            Roles.facebook_status,Roles.secure_status,Roles.paypal_status,Roles.cashapp_status, Roles.venmo_status,
            Roles.zelle_status, Roles.google_play_status,Roles.linked_card_status,favorites2.communication,favorites2.
            product_quality,favorites2.processing_speed,favorites.favourite_id,BaseTbl.follower_id,BaseTbl.following_id
            ,Roles.user_coverimg
            ');
        $this->db->from('tbl_followers as BaseTbl');
        $this->db->join('tbl_users as Roles','Roles.user_id = BaseTbl.user_id');
        $this->db->join('tbl_profile_favourite as favorites','favorites.user_id = BaseTbl.user_id');
         $this->db->join('table_rating as favorites2','favorites2.user_id = BaseTbl.user_id');    
        $this->db->where('BaseTbl.user_id', $user_id);  
        $query = $this->db->get();    
        $result = $query->result();    
        return $result;
    } 
    // .............delete notification.........................


    function remove_notification($id)
    {                            
            $this->db->select ('tbl_notification.notification_id');
            $this->db->from("tbl_notification");
            $this->db->where('user_id',$id);
             $query = $this->db->get();
            $result = $query->result();        
            return $result;  
    }  
    

    function update_notification1($id,$data)
    {                       
        $this->db->where("user_id", $id);
        $this->db->update("tbl_notification", $data);
    }

//.........................................update profile.................................................
     function update_field($id,$data)
     {                       
        $this->db->where("user_id=", $id);
        $this->db->update("tbl_users", $data);
        return TRUE;   
                }



    function updated_data($id)
    {
        $this->db->select ('tbl_users.name,tbl_users.description,tbl_users.user_image');
        $this->db->from("tbl_users");
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        return $query->result(); 
   } 

   // searchh tbl service category..............................................................................


    function search_tblservice_category($searchtext)
    {
        $this->db->select('*');
        $this->db->from('tbl_services_category');
        $likeCriteria = "(name  LIKE '%".$searchtext."%')";
        $this->db->where($likeCriteria);       
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();   
        $result = $query->result();    
        return $result;
    }
    //.....user chat.......................
    function check_add_to_user($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chat');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
    }
    function add_chatUser($data)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_chat', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function insert_report($data)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_report', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function insert_reportuserprofile($data)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_reportuserprofile', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    
    function add_reportUser($data)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_report', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function chat_message($id,$product_id,$receiver_id)
    {
        $this->db->select('*, chat.user_id as userid, tbl_users.name as user_name, chat.createdDtm as msg_time');
        $this->db->from("tbl_chat as chat");
        $this->db->where('chat.user_id', $id);
        $this->db->where('chat.receiver_id', $receiver_id);
        $this->db->where('chat.product_id', $product_id);
        //$this->db->where('chat.raed_status', $product_id);
		$this->db->order_by('chat.createdDtm','DESC'); 
        $this->db->join('tbl_post_item as post','post.id = chat.product_id', 'inner');
        $this->db->join('tbl_users','chat.user_id = tbl_users.user_id');
        $query = $this->db->get();		
		$data= $query->result();
		
		$this->db->select('*, chat.user_id as userid, chat.createdDtm as msg_time');
        $this->db->from("tbl_chat as chat");
        $this->db->where('chat.user_id', $receiver_id);
        $this->db->where('chat.receiver_id', $id);
        $this->db->where('chat.product_id', $product_id);
		$this->db->order_by('chat.createdDtm','DESC'); 
        //$this->db->where('chat.raed_status', $product_id);
        $this->db->join('tbl_post_item as post','post.id = chat.product_id', 'inner');
        // $this->db->join('tbl_users','chat.receiver_id = tbl_users.user_id', 'inner');
        $query2 = $this->db->get();        
		$datareciver = $query2->result();
		
		
		$c=array_merge($data,$datareciver);
		array_multisort(array_map('strtotime',array_column($c,'msg_time')),
                SORT_ASC, $c);
		
		$convertedObj = $this->ToObject($c);		
		
		return $convertedObj;

   }
   public function ToObject($Array) {
      
    // Create new stdClass object
    $object = new stdClass();
      
    // Use loop to convert array into
    // stdClass object
    foreach ($Array as $key => $value) {
        if (is_array($value)) {
            $value = ToObject($value);
        }
        $object->$key = $value;
    }
		return $object;
	}
   
   function chat_message_new($id,$receiver_id)
   {
    $this->db->select('chat.id, chat.read_status, chat.msg_type, chat.product_id,chat.receiver_id,chat.message,chat.createdDtm, post.product_name,post.product_price,post.product_cover_img');
    $this->db->from("tbl_chat as chat");
    $this->db->where('chat.user_id', $id);
    $this->db->where('chat.receiver_id', $receiver_id);
    //$this->db->where('chat.raed_status', $product_id);
    $this->db->join('tbl_post_item as post','post.id = chat.product_id', 'left');
    $this->db->order_by('chat.createdDtm','DESC');     
    $query = $this->db->get();
    $data= $query->row();
    return $data;
   } 
   function chat_message_new_rc($id,$user_id)
   {
    $this->db->select('chat.id, chat.read_status, chat.msg_type, chat.product_id,chat.receiver_id,chat.message,chat.createdDtm, post.product_name,post.product_price,post.product_cover_img');
    $this->db->from("tbl_chat as chat");
    $this->db->where('chat.receiver_id', $id);
    $this->db->where('chat.user_id', $user_id);    
    $this->db->join('tbl_post_item as post','post.id = chat.product_id', 'left');
    $this->db->order_by('chat.createdDtm','DESC');     
    $query = $this->db->get();
    $data= $query->row();
    return $data;
   } 
   function setraed($id,$product_id,$receiver_id)
   {
	   
		$data = array(
			'read_status' => '1',
		);		
		$this->db->where('user_id', $id);
		$this->db->where('receiver_id', $receiver_id);
		$this->db->where('product_id', $product_id);
		$this->db->update('tbl_chat',$data);
		
	   
   }
   function setraedall($id)
   {
	   
		$data = array(
			'read_status' => '1',
		);		
		$this->db->where('user_id', $id);
		$this->db->update('tbl_chat',$data);
		
	   
   }
   function unread_message($id)
    {
       $this->db->select('*');
        $this->db->from("tbl_chat as chat");
        $this->db->where('chat.user_id', $id);
        // $this->db->where('chat.receiver_id', $receiver_id);
        // $this->db->where('chat.product_id', $product_id);
        $this->db->where('chat.read_status',0);
	    $this->db->join('tbl_post_item as post','post.id = chat.product_id', 'left');
        $query = $this->db->get();
		
		$data= $query->result();
		return $data;

   } 
//SELECT DISTINCT receiver_id FROM `tbl_chat` ORDER BY id DESC

	function chat_data_by_reciver($id)
	{
		$this->db->select('BaseTbl.product_id,BaseTbl.user_id');
        $this->db->from('tbl_chat BaseTbl'); 
        $this->db->where('BaseTbl.receiver_id',$id);
        $this->db->group_by('BaseTbl.user_id'); 
        $this->db->order_by('BaseTbl.createdDtm','DESC');     
        $query = $this->db->get(); 
	
        $data= $query->result();
		
        $result=array();
        for($i=0;$i<count($data);$i++)
        {
            $result[]= $this->chat_message_new_rc($id,$data[$i]->user_id);
        }
		return $result;
	}
	function chat_data_by_user($id)
	{
		$this->db->select('BaseTbl.product_id,BaseTbl.receiver_id');
        $this->db->from('tbl_chat BaseTbl'); 
        $this->db->where('BaseTbl.user_id',$id);
        $this->db->group_by('BaseTbl.receiver_id'); 
        $this->db->order_by('BaseTbl.createdDtm','DESC');     
        $query = $this->db->get();		
        $data= $query->result();
        $result=array();
        for($i=0;$i<count($data);$i++)
        {
            $result[]= $this->chat_message_new($id,$data[$i]->receiver_id);
        }
		return $result;

	}
    function chat_data($id,$product_id)
    {
        $query = $this->db->query("select BaseTbl.id, BaseTbl.product_id,BaseTbl.receiver_id,BaseTbl.message,BaseTbl.createdDtm,postItem.product_name,postItem.product_price,postItem.product_cover_img from tbl_chat as BaseTbl Inner join tbl_users as Roles on Roles.user_id = BaseTbl.user_id Inner join tbl_post_item as postItem on postItem.id = BaseTbl.product_id where BaseTbl.user_id = '".$id."' ORDER by BaseTbl.id DESC"); 
        /*$query = $this->db->query("select BaseTbl.id, BaseTbl.product_id,BaseTbl.receiver_id,BaseTbl.message,BaseTbl.createdDtm,postItem.product_name,postItem.product_price,postItem.product_cover_img from tbl_chat as BaseTbl Inner join tbl_users as Roles on Roles.user_id = BaseTbl.user_id Inner join tbl_post_item as postItem on postItem.id = BaseTbl.product_id where BaseTbl.product_id = '".$product_id."' and BaseTbl.user_id = '".$id."' OR BaseTbl.receiver_id = '".$id."' ORDER by BaseTbl.id DESC LIMIT 0,1");*/
        return $query->result();


        //$this->db->distinct(' BaseTbl.receiver_id');DISTINCT(BaseTbl.receiver_id)
        //$this->db->order_by('BaseTbl.id', 'DESC');
         // $this->db->group_by('BaseTbl.message');SELECT max(createdDtm),receiver_id,max(message),max(id),user_id FROM `tbl_chat` group by receiver_id
       
        /*  $this->db->select(' 
                MAX(BaseTbl.createdDtm) as date,
                MAX(BaseTbl.message) as message2,
                BaseTbl.receiver_id,
                Roles.name,
                MAX(BaseTbl.read_status) as ss2,
                BaseTbl.createdDtm,
                Roles.user_image,
                postItem.product_name
            ');*/

       /* $this->db->select('BaseTbl.product_id,BaseTbl.receiver_id,BaseTbl.message,BaseTbl.createdDtm,postItem.product_name,postItem.product_price,postItem.product_cover_img');
        $this->db->from('tbl_chat as BaseTbl');
        // $this->db->group_by('BaseTbl.receiver_id');

        $this->db->join('tbl_users as Roles','Roles.user_id = BaseTbl.user_id', 'INNER');
        $this->db->join('tbl_post_item as postItem','postItem.id = BaseTbl.product_id', 'INNER');

        $this->db->where('BaseTbl.user_id', $id);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit(0, 1);*/

       // $this->db->where_in('createdDtm', max('createdDtm'));  

        

        /*$query = $this->db->get();    
        $result = $query->result();    
        return $result;*/
    } 
    //SELECT * FROM `tbl_chat` WHERE `user_id` = 30 group by receiver_id ORDER BY id DESC
    function report_data($id)
    {
        $this->db->select(' MAX(BaseTbl.createdDtm) as date,MAX(BaseTbl.report_id) as report_id2,BaseTbl.service_id,Roles.name,MAX(BaseTbl.status) as ss2,BaseTbl.createdDtm,Roles.user_image
            ');
        $this->db->from('tbl_report as BaseTbl');
        $this->db->group_by('BaseTbl.service_id');
          


        $this->db->join('tbl_users as Roles','Roles.user_id = BaseTbl.user_id');
        $this->db->where('BaseTbl.user_id', $id);
       // $this->db->where_in('createdDtm', max('createdDtm'));  
        $query = $this->db->get();    
        $result = $query->result();    
        return $result;
    } 








    // search services.................................................................................

 function api_search_services($searchtext)    
    {
        $this->db->select('BaseTbl.service_title,BaseTbl.service_category,
            BaseTbl.latitude,BaseTbl.longitude, Roles.name');
        $this->db->from('tbl_service as BaseTbl');
      $likeCriteria = "(BaseTbl.service_title  LIKE '%".$searchtext."%'
                           OR  BaseTbl.latitude  LIKE '%".$searchtext."%'
                           OR  BaseTbl.longitude  LIKE '%".$searchtext."%'
                           OR  BaseTbl.service_category  LIKE '%".$searchtext."%'
                           OR  BaseTbl.user_id  LIKE '%".$searchtext."%'
                           OR  BaseTbl.createdDtm  LIKE '%".$searchtext."%'
                           OR  BaseTbl.updatedDtm  LIKE '%".$searchtext."%'
                           )";
        $this->db->join('tbl_services_category as Roles','Roles.id = BaseTbl.id');
        $this->db->where($likeCriteria);    
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();   
        $result = $query->result();     
        return $result;
    }

    //.....................................  distance api .........................................

    function product_By_distance($lat,$long)
    {
        $query = $this->db->query("SELECT *, (3959 * acos(cos(radians($lat)) * cos(radians(latitude)) * cos( radians(longitude) - radians($long)) + sin(radians($lat)) * sin(radians(latitude)))) AS distance FROM tbl_post_item HAVING distance < 100 ORDER BY distance LIMIT 0 , 10");
        return $query->result();
        
    }


    // insert rating table data



  /*

    function add_rating($data)
    {
        $this->db->trans_start();
        $this->db->insert('table_rating', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    

    function check_add_to_rating($user_id)
    {
        $this->db->select('*');
        $this->db->from('table_rating');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        
        return $query->row();
    }

    */
    function check_add_to_favourite22($user_id,$receiver_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chat');
        $this->db->where('user_id', $user_id);
         $this->db->where('receiver_id', $receiver_id);
        $query = $this->db->get();     
        return $query->row();
    }



    function add_favourite22($resultdata)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_chat', $resultdata);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

     function update_favourite22($resultdata, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_chat', $resultdata);
        //print_r($this);
        return TRUE;
    }




    //.......table rating2 Add to Favorite..................................................................

    function check_add_to_favourite2($user_id)
    {
        $this->db->select('*');
        $this->db->from('table_rating');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();     
        return $query->row();
    }

    function add_favourite2($resultdata)
    {
        $this->db->trans_start();
        $this->db->insert('table_rating', $resultdata);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function update_favourite2($resultdata, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('table_rating', $resultdata);
        //print_r($this);
        return TRUE;
    }

    //..............total rating by user id..................................................

     function total_Rating($data)
    {
        $this->db->select ('table_rating.rating_id');
        $this->db->from('table_rating');
        $this->db->where('user_id',$data);
        $query = $this->db->get();
        return $query->result(); 
    }  

    //..............total user_rating ..................................................

     function total_users_Rating($data)
    {
        $this->db->select ('tbl_users.rating');
        $this->db->from('tbl_users');
        $this->db->where('user_id',$data);
        $query = $this->db->get();
        return $query->result(); 
    }  



    // ...............tbl_sedrvice data.................................................................


    function views_service_data()
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
       // $this->db->where('id', $user_id);
        //$this->db->where('isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();   
        $result = $query->result();       
        return $result;
    }

    //.........avg of table rating......................................................................

    function avg_table_data($user_id)
    {
        $this->db->select('table_rating.communication,table_rating.product_quality,table_rating.processing_speed');
        $this->db->from('table_rating');
        $this->db->where('user_id', $user_id);
        //$this->db->where('isDeleted', 0);
        //$this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();   
        $result = $query->result();       
        return $result;

        
    }
    //.......profile_favourite............................................

    function check_add_to_profile_favourite($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_profile_favourite');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();  
        return $query->row();
    }



    function add_profile_favourite($resultdata)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_profile_favourite', $resultdata);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function update_profile_favourite($resultdata, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_profile_favourite', $resultdata);
        //print_r($this);
        return TRUE;
    }
    function closest_location_postdistance($lat,$long,$category_id)
    {
        $query = $this->db->query("SELECT *, (6371 * acos(cos(radians($lat)) * cos(radians(latitude)) * cos( radians(longitude) - radians($long)) + sin(radians($lat)) * sin(radians(latitude)))) AS distance FROM tbl_post_item where category_id='".$category_id."' HAVING distance < 15 ORDER BY distance LIMIT 0 , 10");
        return $query->result();        
    }
    
    function closest_location_servicedistance($lat,$long)
    {
        $query = $this->db->query("SELECT *, (6371 * acos(cos(radians($lat)) * cos(radians(latitude)) * cos( radians(longitude) - radians($long)) + sin(radians($lat)) * sin(radians(latitude)))) AS distance FROM tbl_service HAVING distance < 15 ORDER BY distance LIMIT 0 , 10");
        return $query->result();        
    }
    
    function User_profile_data($userId)
    {
        $this->db->select('user_id, name, email, mobile, roleId, user_image');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getLatLong($code){
            // $code = "90011";
           
            $mapsApiKey = 'AIzaSyAb_HEPPAHCQk-s0LtxMjMmlim5_sksXvs';
    		//$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&key=[YOUR KEY]";
            /*echo $query="https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($code)."&output=json&key=".$mapsApiKey;
            echo $new_str = str_replace(' ', '', $query);*/

            //$data = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=160055&sensor=true&output=json&key=AIzaSyCHhzAJ3rjp38H4pgM5JMH3mnAXfBqdToA");

             $data = file_get_contents("https://maps.googleapis.com/maps/api/geocode/xml?sensor=true&address=160055&key=AIzaSyAb_HEPPAHCQk-s0LtxMjMmlim5_sksXvs");
            // if data returned
            if($data){
                // print_r($data);
            // convert into readable format
            $data = json_decode($data);
            $long = $data->Placemark[0]->Point->coordinates[0];
            $lat = $data->Placemark[0]->Point->coordinates[1];
            return array('Latitude'=>$lat,'Longitude'=>$long);
            }else{
            return false;
            }
        }
    
    /* function checkotptokenExist($email, $userId = 0)
    {
        $this->db->select(["email","mobile","password","user_id","device_id","user_image","facebook_id","gmail_id"]);
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("user_id !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    } */
    



    // MARK AS SOLD 
    function check_mark_sold($postId) {
        $this->db->select('active_sold_status,isDeleted');
        $this->db->from('tbl_post_item');
        $this->db->where('id', $postId);
        $query = $this->db->get();
        return $query->row();
        
    }

    function mark_sold($postId,$sold_status) {
        $this->db->where('id', $postId);
        $this->db->update('tbl_post_item', $sold_status);
        
        return TRUE;
    }
    // MARK AS SOLD ENDS HERE

   
    //  Add Service Item 
    function add_service_item($postInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_service_item', $postInfo);        
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();        
        return $insert_id;
    }
    
    // code ends here 

    // Add review and rating
    function add_review_rating($ratingInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_rating', $ratingInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    // code ends here

    // get name by id 
    function getnamebyid($id) {
        $this->db->select('name, user_image');
        $this->db->from('tbl_users');    
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
     function getUserImgById($id) {
        $this->db->select('user_image');
        $this->db->from('tbl_users');    
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    // update_user_profile_image to update profile image using user id
    function update_user_profile_image($userInfo,$user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users', $userInfo);
        $this->db->select('user_image');
        $this->db->from('tbl_users');    
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
    }


    // follower status
    function follower_status ($user_id) {
        $this->db->select('*');
        $this->db->from('tbl_followers');    
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    // check_follower
    function check_follower($user_id,$profile_id) {
        
        $query = $this->db->query("select * from `tbl_followers` WHERE user_id = '".$user_id."' and follower_id = '".$profile_id."' OR following_id = '".$profile_id."'");
        return $query->result();
    }

    /**
     *checkFollowStatus function
     * 
     * This function is called to check if user already following to same user or not.
     * 
     * @access          public
     * @param           
     * @return          params
     * @author          
     */

    public function checkFollowStatus($user_id,$following_to) {
        $this->db->select ('*');
        $this->db->from("tbl_followers");
        $this->db->where('user_id', $user_id);
        $this->db->where('following_id',$following_to);
        $query = $this->db->get();
        return $query->row();
    }


    /*************************************************/
                    /* FAVOURITE API'S  */
    /*************************************************/

    function add_favourite($resultdata)
    {
        $user_id = $resultdata['user_id'];
        $product_id = $resultdata['product_id'];
        $status = 1;
        // to update in post_item table
        $this->db->where('id', $product_id); 
        $this->db->where('user_id', $user_id); 
        
        $fav_status = array('fav_status'=> $status); 
        $this->db->update('tbl_post_item', $fav_status);


        $this->db->trans_start();
        $this->db->insert('tbl_add_to_favourite', $resultdata);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function check_add_to_favourite($user_id,$product_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_add_to_favourite');
        $this->db->where('user_id', $user_id);
        $this->db->where('product_id', $product_id);
        $query = $this->db->get();  
        return $query->row();
    }
    
    function update_favourite($user_id,$product_id,$status)
    {

        if($status==1) {
            $status = 0;
        } else {
            $status = 1;
        }

        // to update in post_item table
        $this->db->where('id', $product_id); 
        $fav_status = array('fav_status'=> $status); 
        $this->db->update('tbl_post_item', $fav_status);

        $this->db->where('user_id', $user_id);
        $this->db->where('product_id', $product_id);   
        $this->db->delete('tbl_add_to_favourite');


        // to get the status of post
        $this->db->select('*');
        $this->db->from('tbl_add_to_favourite');
        $this->db->where('user_id', $user_id);
        $this->db->where('product_id', $product_id);
        $query = $this->db->get();
        return $query->result();

        //print_r($this);
        // return TRUE;
    }
    
    function view_favourite($user_id)
    {
        $this->db->select('fav.*,post.product_cover_img');
        $this->db->from('tbl_add_to_favourite as fav');
        $this->db->join('tbl_post_item as post', 'fav.product_id = post.id', Inner);
        $this->db->where('fav.user_id', $user_id);
        $this->db->where('fav.status', 1);
        $query = $this->db->get();   
        $result = $query->result();  
        return $result;
    }

	 function verifyEmailAddress($verificationcode){  
		$sql = "update tbl_users set Email_status='1' WHERE email_code=?";
		$this->db->query($sql, array($verificationcode));
		return $this->db->affected_rows(); 
	}
    function sendVerificatinEmail($email,$verificationText){
		
		$this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('email', $email);
		$query = $this->db->get();
		if($query->num_rows() < 0)
		{
			return $msg = "Email dosen't exists";
		}
		else {
		$this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('email', $email);
        $this->db->where('Email_status',1);
        $querys = $this->db->get();
		if($querys->num_rows() > 0)
		{
			return $msg = "Email already verified";
		}
		else {
			
		
		
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('pankajkumar.vinnisoft@gmail.com', "Admin Team");
		$this->email->to($email);  
		$this->email->subject("Email Verification");
		$this->email->message("Dear User,\nPlease click on below URL or paste into your browser to verify your Email Address\n\n ".base_url()."/verify/".$verificationText."\n"."\n\nThanks\nAdmin Team");
		$r = $this->email->send();
			if(!$r)
			{
				echo $this->email->print_debugger();
			}
			else {return $msg = "mail sent";}
			
         }
		}
	}
		 


     /*************************************************/
                    /* BIDS API'S  */
    /*************************************************/

    function add_bid($resultdata)
    {    
        $this->db->trans_start();
        $this->db->insert('tbl_bids', $resultdata);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function check_add_to_bid($user_id,$product_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_bids');
        $this->db->where('user_id', $user_id);
        $this->db->where('product_id', $product_id);
        $query = $this->db->get();
        return $query->row();
    }

    function bidresponse($product_id)
    {
        $this->db->select('user.name,user.user_image,bid.bidamount,bid.createdon');
        $this->db->from('tbl_bids as bid');
         $this->db->join('tbl_users as user', 'bid.user_id = user.user_id', Inner);
        $this->db->where('product_id', $product_id);
        $this->db->order_by('bid.id', 'DESC');
        $query = $this->db->get();   
        $result = $query->result();       
        return $result;
    }

    function bid_minmax($product_id)
    {
        $this->db->select('min(bid.bidamount) as min_bid_amount, max(bid.bidamount) as max_bid_amount');
        $this->db->from('tbl_bids as bid');
        $this->db->join('tbl_users as user', 'bid.user_id = user.user_id', Inner);
        $this->db->where('product_id', $product_id);
        $this->db->order_by('bid.id', 'DESC');
        $query = $this->db->get();   
        $result = $query->result();       
        return $result;
    }
    

    function sendSMS($data) {
          // Your Account SID and Auth Token from twilio.com/console
            $sid = 'ACdc7984393a4145c186ed8fa4d36d2231';
            $token = '36c5dd42223730c3b1476ead7e6b2e8a';
            $twilio = new Client($sid, $token); 
            $message = $twilio->messages 
                  ->create($data['phone'], // to 
                           array( 
                               "from" => "+13152458129", 
                               "messagingServiceSid" => "MG3177d05a0012284e51b1df025988a379",      
                               "body" => $data['text']
                           ) 
                  ); 

    }


    function update_otp_token($otpInfo,$userId) {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $otpInfo);
    }

	function countproductbid($countproductbid) {
        $this->db->select('count(*) as totalbid');
        $this->db->from('tbl_bids');
        $this->db->where('product_id', $countproductbid);
        $query = $this->db->get();  
        return $query->row();
    }



    function social_update($id,$data)
    {                       
        $this->db->where("user_id", $id);
        $this->db->update("tbl_users", $data);
        
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where("user_id", $id);
        $query= $this->db->get();
        return $query->result();
      
     }

  /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
   function addNewSocialUser($userInfo)
    {
    
        $this->db->insert('tbl_users', $userInfo);        
        $insert_id = $this->db->insert_id();    

        $this->db->select('*');
        $this->db->from("tbl_users");       
        $this->db->where("isDeleted", 0);
        $this->db->where("user_id", $insert_id); 
        $query = $this->db->get();
        $result = $query->result();
        return  $result;
    }


    //DELETE STATUS
    function check_mark_deleted($postId) {
        $this->db->select('isDeleted');
        $this->db->from('tbl_post_item');
        $this->db->where('id', $postId);
        $query = $this->db->get();
        return $query->row();
        
    }

    function mark_deleted($postId,$delete_status) {
        $this->db->where('id', $postId);
        $this->db->update('tbl_post_item', $delete_status);
        
        return TRUE;
    }
	

    /**
     *add_followers function
     * 
     * This function is called to add new followers.
     * 
     * @access          public
     * @param           $data
     * @return          params
     * @author          
     */

    public function add_followers($data) {        
        $this->db->trans_start();
        $this->db->insert('tbl_followers', $data);
        $this->db->trans_complete();
        return TRUE;
    }

    /**
     *update_followerstatus function
     * 
     * This function is called to follow or unfollow a user.
     * 
     * @access          public
     * @param           $user_id, $follower_id, $status
     * @return          params
     * @author          
     */
    public function update_followerstatus($user_id, $following_to, $status) {
        $resultdata = array('status'=> $status);         
        $this->db->where('user_id', $user_id);
        $this->db->where('following_id', $following_to);
        $this->db->update('tbl_followers', $resultdata);  
        return TRUE;
    }


     /**
     *getUserDetails function
     * 
     * This function is called to get details of a user.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          
     */
    public function getUserDetails($user_id) {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        return $query->row();
    }

	 /**
     *getPostDetails function
     * 
     * This function is called to get details of a post.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          
     */
    public function getPostDetails($post_id) {
        $this->db->select('*');
        $this->db->from('tbl_post_item');
        $this->db->where('isDeleted', 0);
        $this->db->where('id',$post_id);
        $query = $this->db->get();
        return $query->row();
    }

    /**
     *getServiceDetails function
     * 
     * This function is called to get details of a service.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          
     */
    public function getServiceDetails($service_id) {
        $this->db->select('*');
        $this->db->from('tbl_service_item');
        $this->db->where('deleted_at', NULL);
        $this->db->where('id',$service_id);
        $query = $this->db->get();
        return $query->result();
    }


    /**
     *getfollowerFollowingCount function
     * 
     * This function is called to get count of follower and followings of a user.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          
     */
    public function getfollowerFollowingCount($user_id, $column_name) {

        if ($column_name == 'followers') {
            $this->db->select('user_id as followers');
            $this->db->where('following_id',$user_id);
        }elseif ($column_name == 'followings') {
            $this->db->select('following_id as followings');
            $this->db->where('user_id',$user_id);
        }
         $this->db->where('status', 1);
         $this->db->from('tbl_followers');
         $query = $this->db->get();
         return $query->num_rows();
    }
   

    /**
     *updatefollowerFollowing function
     * 
     * This function is called to update followers count and followings count in user table.
     * 
     * @access          public
     * @param           $follower_count, $following_count
     * @return          params
     * @author          
     */
    public function updatefollowerFollowing($user_id, $follower_count, $following_count) {
        $data = array('follower_count'=> $follower_count, 'following_count'=> $following_count);
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users', $data);  
        return TRUE;
    }


    /**
     *updateUserPassword function
     * 
     * This function is called to update followers count and followings count in user table.
     * 
     * @access          public
     * @param           $follower_count, $following_count
     * @return          params
     * @author          
     */
    public function updateUserPassword($user_id, $new_password) {
        $data = array('password'=> $new_password);
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users', $data);  
        return TRUE;
    }

    /**
     *messageCount function
     * 
     * This function is called to get count of total unread message.
     * 
     * @access          public
     * @param           $follower_count, $following_count
     * @return          params
     * @author          
     */
    public function messageCount($user_id, $receiver_id) {
        // $this->db->select('user_id, receiver_id');
        $this->db->select('count(receiver_id) as unreadMsg');
        $this->db->from('tbl_chat');
        $this->db->where('user_id', $user_id);
        $this->db->where('receiver_id', $receiver_id);
        $this->db->where('read_status', 0);
        $this->db->group_by('receiver_id');
        $query = $this->db->get(); 
        return $query->row();
    }


    /**
     *changeOnlineOfflineStatus function
     * 
     * This function is called to update online or offline status if a user.
     * 
     * @access          public
     * @param           user_id, status
     * @return          params
     * @author          
     */
    public function changeOnlineOfflineStatus($user_id, $status) {
        $data = array('status'=> $status);
        $this->db->where('user_id', $user_id);
        $this->db->update('tbl_users_status', $data);  
        return true;
    }


    /**
     *getOnlineOfflineStatus function
     * 
     * This function is called to get online offline status a user.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          
     */
    public function getOnlineOfflineStatus($user_id) {

         $this->db->select('status');
         $this->db->where('user_id', $user_id);
         $this->db->from('tbl_users_status');
         $query = $this->db->get();
         return $query->result_array();
    }

    /**
     *storeOnlineOfflineStatus function
     * 
     * This function is called to get online offline status a user.
     * 
     * @access          public
     * @param           $user_id
     * @return          params
     * @author          
     */
    public function storeOnlineOfflineStatus($user_id, $status) {


        $data = array('user_id' => $user_id, 'status'=> $status);
        $this->db->trans_start();
        $this->db->insert('tbl_users_status', $data);
        $this->db->trans_complete();
        return true;
    }

    /**
     *checkUserChat function
     * 
     * This function is called to check users chat.
     * 
     * @access          public
     * @param           $user_id,$product_id,$receiver_id
     * @return          params
     * @author          
     */
    public function checkUserChat($user_id,$product_id,$receiver_id)
    {
        $this->db->select('*');
        $this->db->from("tbl_chat");
        $this->db->where('user_id', $user_id);
        $this->db->where('receiver_id', $receiver_id);
        $this->db->where('product_id', $product_id);
        $query = $this->db->get();      
        $data= $query->row();

        return $data;

   }


    /**
     * saveChatImages function
     * 
     * This function is called to store chat images.
     * 
     * @access          public
     * @param           $data
     * @return          params
     * @author          
     */
    
    public function saveChatImages($data) {

        $this->db->trans_start();
        $this->db->insert('tbl_chat_images', $data);
        $this->db->trans_complete();
        return true;
    }


    /**
     *getchatImagesOfUser function
     * 
     * This function is called to check users chat.
     * 
     * @access          public
     * @param           $sender_id,$product_id,$receiver_id
     * @return          params
     * @author          
     */
    public function getchatImagesOfUser($sender_id,$product_id,$receiver_id)
    {
        $this->db->select('*');
        $this->db->from("tbl_chat_images");
        $this->db->where('sender_id', $sender_id);
        $this->db->where('receiver_id', $receiver_id);
        $this->db->where('product_id', $product_id);
        $query = $this->db->get();      

        // echo $this->db->last_query(); die();

        $data= $query->row();

        return $data;

   }

    /**
     *updateBid function
     * 
     * This function is called to update.
     * 
     * @access          public
     * @param           user_id, product_id, data
     * @return          params
     * @author          
     */

    public function updateBid($user_id, $product_id, $data) {
        $this->db->where('user_id', $user_id);
        $this->db->where('product_id', $product_id);
        $this->db->update('tbl_bids', $data);
        return true;
    }


    function getChatMessage($id,$product_id,$receiver_id)
    {
        $this->db->select('*, chat.sender_id as userid, chat.created_at as msg_time');
        $this->db->from("tbl_chat_images as chat");
        $this->db->where('chat.sender_id', $id);
        $this->db->where('chat.receiver_id', $receiver_id);
        $this->db->where('chat.product_id', $product_id);
        $this->db->order_by('chat.created_at','DESC'); 
        $query = $this->db->get();      
        $data= $query->result();
        
        $this->db->select('*, chat.sender_id as userid, chat.created_at as msg_time');
        $this->db->from("tbl_chat_images as chat");
        $this->db->where('chat.sender_id', $receiver_id);
        $this->db->where('chat.receiver_id', $id);
        $this->db->where('chat.product_id', $product_id);
        $this->db->order_by('chat.created_at','DESC'); 
        $query2 = $this->db->get();        
        $datareciver = $query2->result();

        $c=array_merge($data,$datareciver);
        array_multisort(array_map('strtotime',array_column($c,'msg_time')),
                SORT_ASC, $c);
        $convertedObj = $this->ToObject($c);    
        
        return $convertedObj;

   }
   



    //..........end of all function here..................................................................
    
}

