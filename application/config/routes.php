<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// $route['default_controller'] = "login";
// $route['404_override'] = 'error_404';
// $route['translate_uri_dashes'] = FALSE;
// $route['verify/(:any)'] = "/Api/verify/$1";


$route['admin/dashboard'] = 'admin/HomeController/index';


/*********** USER DEFINED ROUTES *******************/

// $route['podcast'] = 'user/podcast';
// //$route['watchVideo'] = 'user/watchVideo';
// $route['rjPrograms'] = 'user/rjPrograms';
// $route['notifications'] = 'user/notifications';
// $route['signUp'] = 'user/signUp';
// $route['settings'] = 'user/settings';
// $route['reports'] = 'user/reports';
// $route['loginMe'] = 'login/loginMe';
// $route['dashboard'] = 'user';
// $route['logout'] = 'user/logout';
// $route['userListing'] = 'user/userListing';
// $route['userListing/(:num)'] = "user/userListing/$1";
// $route['addNew'] = "user/addNew";
// $route['addNewUser'] = "user/addNewUser";
// $route['editOld'] = "user/editOld";
// $route['editOld/(:num)'] = "user/editOld/$1";
// $route['editUser'] = "user/editUser";
// $route['deleteUser'] = "user/deleteUser";
// $route['isactivate'] = "user/isactivate";
// $route['profile'] = "user/profile";
// $route['profile/(:any)'] = "user/profile/$1";
// $route['profileUpdate'] = "user/profileUpdate";
// $route['profileUpdate/(:any)'] = "user/profileUpdate/$1";

// //$route['userListingbyuserid'] = "user/userListingbyuserid";
// //$route['userListingbyuserid/(:num)'] = "user/userListingbyuserid/$1";


// $route['loadChangePass'] = "user/loadChangePass";
// $route['changePassword'] = "user/changePassword";
// $route['changePassword/(:any)'] = "user/changePassword/$1";
// $route['pageNotFound'] = "user/pageNotFound";
// $route['checkEmailExists'] = "user/checkEmailExists";
// $route['login-history'] = "user/loginHistoy";
// $route['login-history/(:num)'] = "user/loginHistoy/$1";
// $route['login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";

// $route['forgotPassword'] = "login/forgotPassword";
// $route['resetPasswordUser'] = "login/resetPasswordUser";
// $route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
// $route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
// $route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
// $route['createPasswordUser'] = "login/createPasswordUser";


// $route['emailvarificationUserApi'] = "login/emailvarificationUserApi";
// $route['emailvarificationUserApi/(:any)'] = "login/emailvarificationUserApi/$1";
// $route['emailvarificationUserApi/(:any)/(:any)'] = "login/emailvarificationUserApi/$1/$2";
// $route['createPasswordUserApi'] = "login/createPasswordUserApi";
// $route['loginAPI'] = "login/loginAPI";


// $route['addNewcat'] = "user/addNewcat";
// $route['addNewcat/(:num)'] = "user/addNewcat/$1";
// $route['addNewcategory'] = "user/addNewcategory";
// $route['category'] = "user/category";
// $route['deleteCategory'] = "user/deleteCategory";
// $route['editOldcategory'] = "user/editOldcategory";
// $route['editOldcategory/(:num)'] = "user/editOldcategory/$1";
// $route['editcategory'] = "user/editcategory";
// $route['isactivatecategory'] = "user/isactivatecategory";


// $route['video'] = "user/video";
// $route['video/(:num)'] = "user/video/$1";
// $route['addNewvid'] = "user/addNewvid";
// $route['addNewvideo'] = "user/addNewvideo";
// $route['deleteVideo'] = "user/deleteVideo";
// $route['editOldvideo'] = "user/editOldvideo";
// $route['editOldvideo/(:num)'] = "user/editOldvideo/$1";
// $route['editvideo'] = "user/editvideo";
// $route['isactivatevideo'] = "user/isactivatevideo";


// $route['rj'] = "user/rj";
// $route['rj/(:num)'] = "user/rj/$1";
// $route['addNewrj'] = "user/addNewrj";
// $route['addNewrjs'] = "user/addNewrjs";
// $route['deleteRj'] = "user/deleteRj";
// $route['editOldrj'] = "user/editOldrj";
// $route['editOldrj/(:num)'] = "user/editOldrj/$1";
// $route['editrj'] = "user/editrj";
// $route['isactivaterj'] = "user/isactivaterj";



// $route['program'] = "user/program";
// $route['program/(:num)'] = "user/program/$1";
// $route['addNewpro'] = "user/addNewpro";
// $route['addNewprogram'] = "user/addNewprogram";
// $route['deleteprogram'] = "user/deleteprogram";
// $route['isactivateprogram'] = "user/isactivateprogram";
// $route['editOldprogram'] = "user/editOldprogram";
// $route['editOldprogram/(:num)'] = "user/editOldprogram/$1";
// $route['editprogram'] = "user/editprogram";


// $route['ad_management'] = "user/ad_management";
// $route['ad_management/(:num)'] = "user/ad_management/$1";
// $route['addNewad_manage'] = "user/addNewad_manage";
// $route['addNewad_management'] = "user/addNewad_management";
// $route['deletead_management'] = "user/deletead_management";
// $route['editOldad_management'] = "user/editOldad_management";
// $route['editOldad_management/(:num)'] = "user/editOldad_management/$1";
// $route['editad_management'] = "user/editad_management";


// $route['regis_userinsight'] = "user/regis_userinsight";
// $route['regis_userinsight/(:num)'] = "user/regis_userinsight/$1";

// $route['unregis_userinsight'] = "user/unregis_userinsight";
// $route['unregis_userinsight/(:num)'] = "user/unregis_userinsight/$1";

// $route['stream_fm'] = "user/stream_fm";
// $route['stream_fm/(:num)'] = "user/stream_fm/$1";
// $route['addNewstream'] = "user/addNewstream";
// $route['addNewstream_fm'] = "user/addNewstream_fm";
// $route['deletestreamfm'] = "user/deletestreamfm";
// $route['editOldstreamfm'] = "user/editOldstreamfm";
// $route['editOldstreamfm/(:num)'] = "user/editOldstreamfm/$1";
// $route['editstreamfm'] = "user/editstreamfm";


// /* End of file routes.php */
// /* Location: ./application/config/routes.php */
